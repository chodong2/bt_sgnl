#ifndef _BT_UART_EVENT_H_
#define _BT_UART_EVENT_H_

/* USART peripheral configuration defines */
#define BT_UART					USART2//USART6
#define BT_UART_CLK				RCC_APB1Periph_USART2//RCC_APB2Periph_USART6
#define BT_UART_IRQn				USART2_IRQn//USART6_IRQn

#define BT_UART_GPIO_PORT			GPIOA
#define BT_UART_GPIO_CLK			RCC_AHB1Periph_GPIOA
#define BT_UART_AF				GPIO_AF_USART2//GPIO_AF_USART6

#define BT_UART_TX_PIN				GPIO_Pin_2//GPIO_Pin_9
#define BT_UART_TX_SOURCE			GPIO_PinSource2//GPIO_PinSource9

#define BT_UART_RX_PIN				GPIO_Pin_3//GPIO_Pin_10
#define BT_UART_RX_SOURCE			GPIO_PinSource3//GPIO_PinSource10

#define BT_UART_BAUDRATE				115200
#define BT_UART_BUFFERSIZE				256

void bt_uart_init( void );
void bt_uart_config( void );
void bt_uart_send_byte(uint8_t ui8_byte);
uint8_t bt_uart_read_byte( void );

extern uint8_t device_status;

#define bt_uart_isr					USART2_IRQHandler//USART6_IRQHandler

#endif //_BT_UART_EVENT_H_
