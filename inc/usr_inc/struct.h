#ifndef STRUCT_H
#define STRUCT_H

#include "global_define.h"


typedef enum device_status_{
	
	DEVICE_STANDBY		= 0x0,
	
	DEVICE_NO_PAIRED	= 0x1,
	DEVICE_PAIRING		= 0x2,
	DEVICE_WAIT_EVENT	= 0x3,
	DEVICE_INCOMING_CALL	= 0x4,
	DEVICE_OUTGOING_CALL	= 0x5,
	DEVICE_CALLING		= 0x6,
	DEVICE_FAVORITE_CALL	= 0x9,
	DEVICE_OFF		= 0xE,
	DEVICE_DISCONNECTED	= 0xF,
}device_status_t;

extern device_status_t g_device_status;

typedef enum power_mode_{
	
	PWR_MODE_NONE			= 0x0,
	PWR_MODE_LOW			= 0x1,
	PWR_MODE_HIGH			= 0x2,
	
}power_mode_t;

typedef struct timer_struct_{
	
	volatile uint16_t ui16_audio_cnt;
	volatile uint16_t ui16_bt_power_off_cnt;
	
	volatile uint16_t ui16_sleep_cnt;
	volatile uint16_t ui16_long_press_cnt;
	
#ifdef ENABLE_BLE_UART
	volatile uint16_t ui16_ble_delay_cnt;
	volatile uint16_t ui16_bt_wakeup_cnt;
#endif	
	
#ifdef ENABLE_BT_UART
	volatile uint16_t ui16_bt_pairing_cnt;
#endif
#ifdef ENABLE_SIMPLE_BT
	volatile uint16_t ui16_mfb_cnt;
	volatile uint16_t ui16_make_call_cnt;
	volatile uint16_t ui16_nrst_cnt;
#endif
}timer_struct_t;

extern timer_struct_t g_str_timer;

#ifdef ENABLE_AUDIO
typedef enum audio_state_{
	
	AUDIO_STATE_STOP		= 0,
	AUDIO_STATE_PLAY		= 1,
	
}audio_state_t;

typedef enum audio_frame_{
	
	AUDIO_FRAME_OK = 0,
	AUDIO_FRAME_ERR = 1,
	
}audio_frame_t;

typedef enum audio_channel_{
	
	AUDIO_CHANNEL_NONE		= 0,
	AUDIO_CHANNEL_LEFT		= 0,
	AUDIO_CHANNEL_RIGHT		= 1,
	AUDIO_CHANNEL_TOGGLE	= 1,
	
}audio_channel_t;

typedef enum audio_input_freq_{
	
	AUDIO_INPUT_NONE = I2S_AudioFreq_8k,
	AUDIO_INPUT_8K = I2S_AudioFreq_8k,
	AUDIO_INPUT_16K = I2S_AudioFreq_16k,
	AUDIO_INPUT_44_1K = I2S_AudioFreq_44k,
	AUDIO_INPUT_48K = I2S_AudioFreq_48k,
	
}audio_input_freq_t;

#ifdef ENABLE_FREQ_CHECK
typedef enum freq_status_{

	FREQ_CHK_READY		= 0x0,
	FREQ_CHK_START		= 0x1,
	FREQ_CHK_ING		= 0x2,
	FREQ_CHK_ANALYZE	= 0x3,
	FREQ_CHK_END		= 0x4,
	
}freq_status_t;
#endif

typedef struct audio_struct_{
	
#ifdef ENABLE_FE
	volatile uint16_t ui16_save_buf[FE_BUFFER][AUDIO_FRAME_SIZE];
#else
	volatile uint16_t ui16_save_buf[AUDIO_FRAME_BUFFER][AUDIO_FRAME_SIZE];
#endif
	volatile uint16_t ui16_save_cnt;
	volatile uint16_t ui16_freq_cnt;
	volatile uint16_t ui16_sec_cnt;

	volatile uint16_t ui16_rx_frame_no;
	volatile uint16_t ui16_tx_frame_no;
	
	volatile audio_input_freq_t	ui16_pre_freq;
	
	volatile audio_input_freq_t ui16_audio_type;

#ifdef ENABLE_VOL_SHIFT
	volatile uint16_t ui16_vol_level;
#endif	
}audio_struct_t;
extern audio_struct_t g_str_audio;

#ifdef ENABLE_FE
typedef struct fe_struct_{
	
	volatile uint16_t ui16_frame_no;
	volatile uint16_t ui16_data[FE_BUFFER][AUDIO_FRAME_SIZE];
	
}fe_struct_t;

extern fe_struct_t g_str_fe;
#endif //ENABLE_FE

#endif //ENABLE_AUDIO

#ifdef ENABLE_BT_UART
typedef struct bt_uart_struct_{
	
	volatile uint8_t	ui8_rx_buf[BT_UART_BUFFERSIZE];
	volatile uint16_t	ui16_rx_head;
	volatile uint16_t	ui16_rx_tail;
	volatile uint16_t	ui16_rx_cnt;
	volatile uint16_t	ui16_rx_flag;
	
	volatile uint8_t	ui8_tx_buf[BT_UART_BUFFERSIZE];
	volatile uint16_t	ui16_tx_head;
	volatile uint16_t	ui16_tx_tail;
	volatile uint16_t	ui16_tx_cnt;
	volatile uint16_t	ui16_tx_flag;
	
	volatile uint16_t 	ui16_bt_mode_count;
}bt_uart_struct_t;

extern bt_uart_struct_t g_str_bt;
#endif //ENABLE_BT_UART

#ifdef ENABLE_BLE_UART
extern bt_uart_struct_t g_str_ble;
#endif

#ifdef ENABLE_OTA

typedef enum ota_state_{
	
	OTA_STATE_NONE			= 0,
	OTA_STATE_INITIAL		= 1,
	OTA_STATE_CHECK_VERSION	= 2,
	OTA_STATE_CHECK_SIZE	= 3,
	OTA_STATE_UPDATE		= 4,
	OTA_STATE_DATA_END		= 5,
	OTA_STATE_SUCCESS		= 6,
	OTA_STATE_FAIL			= 7,
	OTA_STATE_END			= 8,
	OTA_STATE_READY			= 9,
	
}ota_state_t;


typedef struct ota_struct_{
	volatile ota_state_t ui16_state;
}ota_struct_t;

extern ota_struct_t g_str_ota;

#endif //ENABLE_OTA

typedef enum BT_SystemStatus_e_{
	BT_SYSTEM_INIT,         //init
	BT_SYSTEM_POWER_OFF,    //event
	BT_SYSTEM_POWER_ON,     //event
	BT_SYSTEM_STANDBY,      //event
	BT_SYSTEM_CONNECTED,    //event
	BT_SYSTEM_PAIRING,      //event
} BT_SystemStatus_e;        // BM64 internal system status
extern BT_SystemStatus_e BT_SystemStatus;

typedef enum BT_LinkbackStatus_e_{
	BT_LINKBACK_INIT,       //init
	BT_LINKBACK_CONNECTING, //sent link_back command but no event yet
	BT_LINKBACK_OK,         //link back success, event
	BT_LINKBACK_FAILED,     //linkback failed, event
	BT_PAIRING_START,       //sent pair command but no event yet
	BT_PAIRING,             //pair event
	BT_PAIRING_OK,          //pair event
	BT_PAIRING_FAILED,      //pair event
	BT_LINK_CONNECTED,         //other event
} BT_LinkbackStatus_e;       // linkback status
extern BT_LinkbackStatus_e BT_LinkbackStatus;

typedef enum BT_CallStatus_e_{
	BT_CALL_IDLE = 0,
	BT_VOICE_DIAL = 1,
	BT_CALL_INCOMING = 2,
	BT_CALL_OUTGOING = 3,
	BT_CALLING = 4,
	BT_CALLING_WAITING = 5,
	BT_CALLING_HOLD = 6,
	BT_CALLING_INIT = 7
} BT_CallStatus_e;
extern BT_CallStatus_e BT_CallStatus;

typedef enum bt_power_status_e_{
	BT_POWER_OFF	= 0x00,
	BT_POWER_ON	= 0x01,
} bt_power_status_e;
extern bt_power_status_e bt_power_status;


typedef enum bt_bt_bt_pairing_status_e_{
	PAIRING_INIT	= 0x00,
	PAIRING_START	= 0x01,
	PAIRING_TIMEOUT	= 0x02,
	PAIRING_SUCCESS	= 0x03,
	PAIRING_FAIL	= 0x04,
} bt_pairing_status_e;
extern bt_pairing_status_e bt_pairing_status;


typedef enum mcu_status_e_{
	MCU_SLEEP	= 0x00,
	MCU_READY	= 0x01,
} mcu_status_e;
extern mcu_status_e mcu_status;


typedef enum bt_call_status_e_{
	BT_CALL_INIT	= 0x00,
	OUTGOING_CALL	= 0x01,
	CALLING		= 0x02,
	END_CALL	= 0x03,
} bt_call_status_e;
extern bt_call_status_e bt_call_status;


typedef enum mcu_to_ble_data_e_{
	MCU_BLE_DATA		= 0x00,
	BT_POWER		= 0x01,
	BT_PAIRING_STATUS	= 0x02,
	MCU_STATUS		= 0x03,
	BT_CALL_STATUS		= 0x04,
}mcu_to_ble_data_e;
extern mcu_to_ble_data_e mcu_to_ble_data;


typedef enum ble_call_command_e_{
	CALL_INIT_CALL		= 0x00,
	CALL_INCOMING_CALL	= 0x01,
	CALL_RECEIVE_CALL	= 0x02,
	CALL_END_CALL		= 0x03,
	CALL_MAKE_CALL		= 0x04,
	CALL_OUTGOING_CALL	= 0x05,
} ble_call_command_e;
extern ble_call_command_e ble_call_command;

#ifdef ENABLE_VOL_SHIFT
typedef enum volume_control_e_{
	VOLUME_INIT		= 0x00,
	VOLUME_UP		= 0x01,
	VOLUME_DOWN		= 0x02,
} volume_control_e;
extern volume_control_e volume_control;
#endif

typedef enum ble_report_e_{
	BLE_REPORT_INIT			= 0x00,
	BLE_REPORT_BT_PAIRING_ON	= 0x01,
	BLE_REPORT_CALL_STATUS		= 0x02,
	BLE_REPORT_VOLUME_CONTROL	= 0x03,
	BLE_REPORT_DEFAULT_RESET	= 0x04,
}ble_report_e;
extern ble_report_e ble_report;

typedef struct bitflag_struct_{

#ifdef ENABLE_AUDIO
	volatile audio_frame_t b1_frame_state:1;
	
	volatile audio_state_t b1_audio_rx_state:1;
	volatile audio_state_t b1_audio_tx_state:1;
	volatile uint16_t b1_audio_stream_flag:1;
#endif
	volatile uint16_t b1_bt_conn:1;

#ifdef ENABLE_FE
	volatile uint16_t b1_fe_enable:1;
	volatile uint16_t b2_fe_start_flag:2;
#endif

	volatile device_status_t b4_device_status:4;
	
#ifdef ENABLE_BLE_UART
	volatile uint16_t b1_incoming_call:1;
#endif
	volatile uint16_t b1_audio_play:1;
	
#ifdef ENABLE_FREQ_CHECK
	volatile freq_status_t b3_freq_chk:3;
#endif
	volatile uint8_t b1_power_down:1;
	
	volatile ble_report_e b3_ble_report:3;
	volatile ble_call_command_e b3_ble_call_command:3;
	volatile bt_pairing_status_e b3_bt_status:3;
	volatile uint8_t b2_make_call_status:2;
#ifdef ENABLE_SIMPLE_BT
	volatile uint16_t b1_nrst_flag:1;
	volatile uint16_t b2_bt_on_off:2;
	volatile uint16_t b1_pairing_cmd:1;
#endif
	volatile uint16_t b1_pairing_flag:1;
	volatile BT_CallStatus_e b3_real_call_status:3;
}bitflag_struct_t;

extern bitflag_struct_t g_str_bitflag;

extern uint16_t g_ui16_loop_cnt;
extern uint8_t g_ui8_phone_num[20];

#endif //STRUCT_H
