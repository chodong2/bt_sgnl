/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _FLASH_H
#define _FLASH_H

/* Exported functions ------------------------------------------------------- */
uint32_t GetSector(uint32_t Address);
extern void flash_erase(uint32_t start_sec, uint32_t end_sec);
extern void flash_write_32(uint32_t st_addr, uint32_t *variable, uint32_t inter, uint32_t size);
extern void flash_read_32(uint32_t st_addr, uint32_t *variable, uint32_t inter, uint32_t size);

extern void flash_write_16(uint32_t st_addr, uint16_t *variable, uint16_t inter, uint16_t size);
extern void flash_read_16(uint32_t st_addr, uint16_t *variable, uint16_t inter, uint16_t size);


#endif /* _FLASH_H */
