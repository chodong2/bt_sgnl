#ifndef GLOBAL_DEFINE_H
#define GLOBAL_DEFINE_H


/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */	// bootloader
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */	// pedometer data
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */	// favorite call data
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */	// jump app
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */	// user app1 data
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */	//           data2
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */	// user app2 data
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */	//           data2
#define ADDR_FLASH_SEC_END      ((uint32_t)0x08080000) /* Base @ of Sector 7, 128 Kbytes */	//           data2

#define	FLASH_ADDR_JUMP_APP			ADDR_FLASH_SECTOR_3

#define	FLASH_ADDR_DATA_LOG			( ADDR_FLASH_SECTOR_3 + 0x10 )
#define	FLASH_ADDR_DATA_LOG_END		ADDR_FLASH_SECTOR_4

#define	ERASE_LOG_START_SECTOR	FLASH_Sector_3
#define	ERASE_LOG_END_SECTOR	FLASH_Sector_4


#define	FLASH_ADDR_START_APP1		ADDR_FLASH_SECTOR_4
#define	FLASH_ADDR_END_APP1			ADDR_FLASH_SECTOR_6
#define	FLASH_ADDR_START_APP2		ADDR_FLASH_SECTOR_6
#define	FLASH_ADDR_END_APP2			ADDR_FLASH_SEC_END

#define FLASH_ADDR_START_PEDO   	ADDR_FLASH_SECTOR_1   /* Start @ of user Flash area */
#define FLASH_ADDR_END_PEDO     	ADDR_FLASH_SECTOR_2   /* End @ of user Flash area */

#define FLASH_ADDR_START_FAVO   	ADDR_FLASH_SECTOR_2   /* Start @ of user Flash area */
#define FLASH_ADDR_END_FAVO     	ADDR_FLASH_SECTOR_3   /* End @ of user Flash area */


#if defined(CURRENT_APP_1)
#define	FLASH_ADDR_CURRENT_APP		FLASH_ADDR_START_APP1
#define	FLASH_ADDR_UPDATE_APP		FLASH_ADDR_START_APP2
#endif

#if defined(CURRENT_APP_2)
#define	FLASH_ADDR_CURRENT_APP		FLASH_ADDR_START_APP2
#define	FLASH_ADDR_UPDATE_APP		FLASH_ADDR_START_APP1
#endif

#if defined(CURRENT_APP)
#define	FLASH_ADDR_CURRENT_APP		ADDR_FLASH_SECTOR_0
#define	FLASH_ADDR_UPDATE_APP		FLASH_ADDR_START_APP2
#endif


#if defined(CURRENT_APP_1)
#define ERASE_START_SECTOR		FLASH_Sector_6
#define ERASE_END_SECTOR		FLASH_Sector_8

#endif

#if defined(CURRENT_APP_2)

#define ERASE_START_SECTOR		FLASH_Sector_4
#define ERASE_END_SECTOR		FLASH_Sector_6	

#endif

#if defined(CURRENT_APP)
#define ERASE_START_SECTOR		FLASH_Sector_6
#define ERASE_END_SECTOR		FLASH_Sector_8

#endif

/* pre define module */

//#define ENABLE_KEY_INPUT
#define ENABLE_MONITOR

#define ENABLE_SIMPLE_BT
//#define ENABLE_BT_TEST
#define ENABLE_IS206x

#define ENABLE_BT_UART
#define ENABLE_BLE_UART
//#define ENABLE_AUDIO
//#define ENABLE_AUDIO_DMA
#define ENABLE_STANDBY
#define ENABLE_FAST_CALL
//#define ENABLE_FREQ_CHECK

//#define ENABLE_OTA
//#define ENABLE_FE

//#define DBG_PRINT

#define OLD_VER_BOARD

#ifndef OLD_VER_BOARD
#define TEST_BOARD
#endif

//#define OLD_PROTOCOL

//#define CES_DEMOCODE

#define ENABLE_VOL_SHIFT

#ifdef ENABLE_STANDBY
#define SLEEP_COUNT     	100
#define WAKEUP_SLEEP_COUNT	500
#define PAIRING_COUNT		6000
#endif

#include "main.h"

#ifdef ENABLE_SIMPLE_BT
#include "simple_bt_app.h"
#endif
#ifdef ENABLE_IS206x
#include "bt_app.h"
#include "bt_command_send.h"
#include "bt_command_decode.h"

#include "uart_parser.h"
#else
#undef ENABLE_OTA
#endif //ENABLE_IS206x

/* include header files */
#include "stm32f4xx.h"

#include "pin_enable.h"
#include "timer_event.h"

#ifdef ENABLE_BT_UART
#include "bt_uart_event.h"
#endif //ENABLE_BT_UART

#ifdef ENABLE_BLE_UART
#include "ble_uart_event.h"
#include "ble_protocol.h"
#endif //ENABLE_BLE_UART

#ifdef ENABLE_LED
#include "led_event.h"
#endif //ENABLE_LED

#ifdef ENABLE_AUDIO
#include "audio_event.h"
#endif //ENABLE_AUDIO

#ifdef ENABLE_FE
#include "fe_init.h"
#endif //ENABLE_FE

#ifdef ENABLE_OTA
#include "ota_event.h"
#endif //ENABLE_OTA

#ifdef ENABLE_STANDBY
#include "low_power.h"
#include "tm_stm32f4_rcc.h"
#endif //ENABLE_STANDBY


#include "struct.h"

#endif //GLOBAL_DEFINE_H
