#ifndef _DATA_LOG_H_
#define _DATA_LOG_H_

typedef enum datalog_t_{
	
	// power on/off
	LOG_POWER_ON				= 0x00,//
	LOG_POWER_OFF_USER			= 0x01,//
	LOG_POWER_OFF_BATT			= 0x02,//
	
	// initailize
	LOG_INITIALIZE_OK			= 0x03,//
	LOG_POWER_ON_RESET			= 0x04,//
	LOG_WATCHDOG_RESET			= 0x05,//
	
	// charger & battery
	LOG_CHARGER_ENABLE			= 0x10,//
	LOG_CHARGER_DISABLE			= 0x11,//
	LOG_CHARGE_COMPLETE			= 0x12,//
	LOG_CHARGE_FAIL				= 0x13,//
	LOG_LOWBATT_ALARM			= 0x14,//
	
	// BT / BLE
	LOG_ENTER_PAIRING			= 0x20,//
	LOG_PAIRING_SUCCESS			= 0x21,//
	LOG_PAIRING_FAIL			= 0x22,//
	
	LOG_HFP_CONNECTED			= 0x23,//
	LOG_HFP_DISCONNECTED		= 0x24,//
	
	LOG_BLE_ADVERTISING			= 0x25,//
	LOG_BLE_CONNECTED			= 0x26,//
	LOG_BLE_DISCONNECTED		= 0x27,//
	LOG_BLE_ANCS_FOUND			= 0x28,//
	
	// sgnl function
	LOG_CALL_STATUS_INCOM		= 0x30,//
	LOG_CALL_STATUS_OUTGO		= 0x31,//
	LOG_CALL_STATUS_CALLING		= 0x32,//
	LOG_CALL_STATUS_REJECT		= 0x33,//
	LOG_CALL_STATUS_END_CALL	= 0x34,//
	
	LOG_DO_NOT_DISTURB_ON		= 0x40,
	LOG_DO_NOT_DISTURB_OFF		= 0x41,
	
	LOG_LED_FEEDBACK_ON			= 0x50,
	LOG_LED_FEEDBACK_OFF		= 0x51,
	
	LOG_HAPTIC_FEEDBACK_ON		= 0x60,
	LOG_HAPTIC_FEEDBACK_OFF		= 0x61,
	
	LOG_TIME_SYNC				= 0x70,//
	
}datalog_t;

uint8_t load_logcnt_flash( void );
uint8_t write_log_flash( datalog_t ui8_log );
uint8_t reset_log_flash( void );
uint8_t print_log_flash( void );

#endif //_DATA_LOG_H_