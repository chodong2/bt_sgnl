#ifndef ECHO_CANCEL_H
#define ECHO_CANCEL_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Common.h"

#define H_FFTSIZE 64
#define	NUM_CHAN 16
#define	HI_CHAN 15

#define	LO_CHAN 0
#define	MID_CHAN 5
#define MINSNR 0.0
#define MINGAIN 0.0

#define MIN_CHAN_ENRG (0.0625 / 1.0)
#define epsi 1.0

#define	ns_MINSNR 0.085

#define para_a 1.0             // sharpness
#define para_b 1.0 
#define para_minimum 0.0     // minimum floor

#define	NMAX 20
#define  SQ(x) ((x) * (x))
#define  SQRT(x) (float)sqrt((double)(x))
#define  EXP(x) (float)exp((double)(x))
#define  MIN1(x,y) (x < y) ? x : y
#define  MIN2(x,y,z) (((x < y)? x : y ) < z ) ? ((x < y)? x : y) : z
#define  MAX1(x,y) (x > y ) ? x : y
#define  MAX2(x,y,z) (((x > y)? x : y ) > z ) ? ((x > y)? x : y) : z

void Echo_suppress(short x[], short y[], short out_buf[]);


#endif
