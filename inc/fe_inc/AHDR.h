#ifndef AHDR_H
#define AHDR_H

#include "Common.h"

#define ALPHA_ATTACK                 16775462 //0.999895535856168, Q24
#define ALPHA_RELEASE                7097604  //0.423050189619513, Q24

#define TN                           200
#define TE                           15000
#define TC                           20000
#define TL                           25000
#define MAX_VAL                      32767
#define UP_SMOOTHING                 1932735282   //0.90, Q.31
#define DOWN_SMOOTHING               644245094    //0.30, Q.31
#define INV_FRAMESIZE                3277      //1/FRAMESIZE, Q.18

#define AHDR_VAL0                      72509917 //4.321928e+00, Q.24
#define AHDR_VAL1                      -16777215 //-1, Q.24
#define AHDR_VAL2                      128242619 //7.643856e+00, Q.24
#define AHDR_VAL3                      10409440 //6.204510e-01, Q.24
#define AHDR_VAL4                      245109079 //1.460964e+01, Q.24
#define AHDR_VAL5                      12352520 //7.362676e-01, Q.24
#define AHDR_VAL6                      246058393 //1.466622e+01, Q.24
#define AHDR_VAL7                      -2550189 //-1.520031e-01, Q.24
#define AHDR_VAL8                      16777215 //1, Q.24
#define AHDR_VAL9                      249522064 //1.487267e+01, Q.24


int reset_AHDR(void);
int AcousticHighDynamicRange_exe(short *x, int *options, int l);


extern int LogTable[];
extern short Pow_tab[];
extern int IirCoeff[][6];
#endif
