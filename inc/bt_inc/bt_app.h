/******************************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PICmicro(r) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PICmicro Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
********************************************************************/
#ifndef BT_APP_H
#define BT_APP_H

#include <stdbool.h>
#include <stdint.h>

void BTAPP_Init( void );
void BTAPP_Task( void );
void nextCommandReqCheck(void);
void BTAPP_Timer1MS_event( void );
// @ request define
typedef enum bt_request_e_{
    BT_REQ_NONE = 0,
    BT_REQ_SYSTEM_ON,   //BM64 power on request(outside application must control RESET and MFB timing, then call this request)
    BT_REQ_SYSTEM_OFF,  //BM64 power off request(after call this, outside application must control RESET and MFB)
}bt_request_e;
void BTAPP_TaskReq(uint8_t request);

// @ event define
typedef enum bt_event_e_{
    BT_EVENT_NONE = 0,

    BT_EVENT_NSPK_STATUS,               //1
    BT_EVENT_LINE_IN_STATUS,            //2
    BT_EVENT_A2DP_STATUS,               //3
    BT_EVENT_CALL_STATUS_CHANGED,       //4

    BT_EVENT_HFP_CONNECTED,             //5
    BT_EVENT_HFP_DISCONNECTED,          //6
    BT_EVENT_A2DP_CONNECTED,            //7
    BT_EVENT_A2DP_DISCONNECTED,         //8
    BT_EVENT_AVRCP_CONNECTED,           //9
    BT_EVENT_AVRCP_DISCONNECTED,        //10
    BT_EVENT_SPP_CONNECTED,             //11
    BT_EVENT_IAP_CONNETED,              //12
    BT_EVENT_SPP_IAP_DISCONNECTED,      //13
    BT_EVENT_ACL_CONNECTED,             //14
    BT_EVENT_ACL_DISCONNECTED,          //15
    BT_EVENT_SCO_CONNECTED,             //16
    BT_EVENT_SCO_DISCONNECTED,          //17
    BT_EVENT_MAP_CONNECTED,             //18
    BT_EVENT_MAP_DISCONNECTED,          //19

    BT_EVENT_SYS_POWER_ON,              //20
    BT_EVENT_SYS_POWER_OFF,             //21
    BT_EVENT_SYS_STANDBY,               //22
    BT_EVENT_SYS_PAIRING_START,         //23
    BT_EVENT_SYS_PAIRING_OK,            //24
    BT_EVENT_SYS_PAIRING_FAILED,        //25

    BT_EVENT_LINKBACK_SUCCESS,          //26
    BT_EVENT_LINKBACK_FAILED,           //27

#if 1 //SH_ADD
	BT_EVENT_READ_LINK_STATUS,      //28
	BT_EVENT_REPORT_TYPE_CODEC,     //29
#endif
	
    BT_EVENT_BD_ADDR_RECEIVED,          //30
    BT_EVENT_PAIR_RECORD_RECEIVED,      //31
    BT_EVENT_LINK_MODE_RECEIVED,        //32        

    BT_EVENT_PLAYBACK_STATUS_CHANGED,   //33
    BT_EVENT_AVRCP_VOLUME_CTRL,         //34
    BT_EVENT_AVRCP_ABS_VOLUME_CHANGED,  //35
    BT_EVENT_HFP_VOLUME_CHANGED,        //36        
    
    NSPK_EVENT_SYNC_POWER_OFF,          //37
    NSPK_EVENT_SYNC_VOL_CTRL,           //38
    NSPK_EVENT_SYNC_INTERNAL_GAIN,      //39
    NSPK_EVENT_SYNC_ABS_VOL,            //40
    NSPK_EVENT_CHANNEL_SETTING,         //41
    NSPK_EVENT_ADD_SPEAKER3,            //42        
#if 1 //SH_ADD
	
	BT_EVENT_BTM_BATT_LEVEL,        //43
	BT_EVENT_BTM_CHG_TYPE,          //44
	LE_ANCS_REPORT,                 //45
#endif
    LE_STATUS_CHANGED,                  //46
    LE_ADV_CONTROL_REPORT,              //47        
    LE_CONNECTION_PARA_REPORT,          //48
    LE_CONNECTION_PARA_UPDATE_RSP,      //49
    GATT_ATTRIBUTE_DATA,                //50
    
    PORT0_INPUT_CHANGED,                //51
    PORT1_INPUT_CHANGED,                //52
    PORT2_INPUT_CHANGED,                //53
    PORT3_INPUT_CHANGED,                //54
}bt_event_e;

enum {
    BT_STATUS_NONE,
    BT_STATUS_OFF,
    BT_STATUS_ON,
    BT_STATUS_READY
};

uint8_t BTAPP_GetStatus(void);

void BTAPP_EventHandler(bt_event_e event, uint16_t para, uint8_t* para_full);

void BTAPP_PowerOffMode( void );
void BTAPP_PowerOnMode( void );
void BTAPP_EnterBTPairingMode( void );
void BTAPP_ResetEEPROMtoDefault( void );
void BTAPP_PlayNextSong( void );
void BTAPP_PlayPreviousSong( void );
void BTAPP_PlayPause( void );
void BTAPP_StartFastForward( void );
void BTAPP_StartFastRewind( void );
void BTAPP_CancelForwardOrRewind( void );
void BTAPP_BroadcastBtnLongPress(void);
void BTAPP_BroadcastBtnDbClick(void);
void BTAPP_ExitBroadcastRegisterMode( void );
void BTAPP_GroupSpeakerSoundSync( void );
void BTAPP_NSPKBtnLongPress( void );
void BTAPP_NSPKBtnDbClick( void );
void BTAPP_NSPKBtnShortPress( void );
void BTAPP_CallEventShort( void );
void BTAPP_CallEventLong(void);
void BTAPP_VolUp( void );
void BTAPP_VolDown( void );

bool getI2SAuxInJumper( void );
bool getDatabase3Jumper( void );
//SPP data exchange
void BT_SPPBuffClear( void );
bool BTAPP_SendDataOverSPP(uint8_t* addr, uint32_t size);
bool BTAPP_SendCharOverSPP(uint8_t byte);
bool BT_AddBytesToSPPBuff(uint8_t* data, uint8_t size);
bool BT_AddBytesToSPPBuffFromBeginning(uint8_t* data, uint8_t size, bool data_True_Command_False)  ;      //test
void SPPTask(void);                                 //test
uint8_t IsSPPTaskIdle(void);
bool BT_ReadOneByteFromSPPBuff( uint8_t* byte );
void BT_SaveLocalBDAddress(uint8_t* address);
void BT_SaveMasterBDAddress(uint8_t* address);
bool BT_CustomerGATT_AttributeData(uint8_t attributeIndex, uint8_t* attributeData, uint8_t attributeDataLength);

extern uint8_t BT_linkIndex;
extern uint16_t BTAPP_timer1ms;
#endif
