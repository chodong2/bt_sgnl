#ifndef _OTA_EVENT_H_
#define _OTA_EVENT_H_

#define	MAX_GET_DATA_SIZE	16

void ota_parser( uint8_t* u8_data, uint8_t u8_size );
void receive_ota( uint8_t* u8_data, uint8_t u8_index );

void JumpToUpdateApp( void );

void OTA_Task( void );

#endif //_OTA_EVENT_H_
