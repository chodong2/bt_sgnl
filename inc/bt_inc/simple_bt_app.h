#ifndef _SIMPLE_BT_APP_H_
#define _SIMPLE_BT_APP_H_

typedef enum
{
	SIMPLE_BT_OFF = 0x00,
	SIMPLE_BT_DISCOVERABLE = 0x01,
	SIMPLE_BT_ON = 0x02,
	SIMPLE_BT_PAIRING_COMPLETE = 0x03,
	SIMPLE_BT_PAIRING_FAIL = 0x04,
	SIMPLE_BT_PAIRING_TIMEOUT	= 0x05,
	SIMPLE_BT_HFP_CONNECTED		= 0x06,
	SIMPLE_BT_A2DP_CONNECTED	= 0x07,
	SIMPLE_BT_HFP_DISCONNECTED	= 0x08,	
	SIMPLE_BT_A2DP_DISCONNECTED	= 0x09,
	SIMPLE_BT_SCO_CONNECTED		= 0x0A,
	SIMPLE_BT_SCO_DISCONNECTED	= 0x0B,
	SIMPLE_BT_ARVCP_CONNECTED	= 0x0C,
	SIMPLE_BT_AVRCP_DISCONNECTED	= 0x0D,
	SIMPLE_BT_SPP_CONNECTED		= 0x0E,
	SIMPLE_BT_SPP_DISCONNECTED	= 0x0F,
	SIMPLE_BT_STANDBY		= 0x10,
	SIMPLE_BT_iAP_CONNECTED		= 0x11,
	SIMPLE_BT_ACL_DISCONNECTED	= 0x12,
	SIMPLE_BT_MAP_CONNECTED		= 0x13,
	SIMPLE_BT_MAP_FORBIDDEN		= 0x14,
	SIMPLE_BT_MAP_DISCONNECTED	= 0x15,
	SIMPLE_BT_ACL_CONNECTED		= 0x16,

}simple_bt_status;

void bt_simple_task( void );
void bt_simple_pairing_on( void );
void bt_simple_power_on( void );
void bt_simple_power_off( void );
void bt_simple_end_call( void );
void bt_simple_default_reset( void );
void bt_simple_clear_delay( void );
void bt_make_call( void );

extern simple_bt_status simple_status;

#endif //_SIMPLE_BT_APP_H_