#include "global_define.h"

#ifdef ENABLE_SIMPLE_BT

#define SIMPLE_TICK			8 //80ms
#define SIMPLE_POWER_ON_DELAY		SIMPLE_TICK*5
#define SIMPLE_POWER_OFF_DELAY		SIMPLE_TICK*25
#define SIMPLE_PAIRING_ON_DELAY		SIMPLE_TICK*15
#define SIMPLE_LONG_KEY_DELAY		SIMPLE_TICK*25
#define SIMPLE_DEFAULT_RESET_DELAY	SIMPLE_TICK*50
#define SIMPLE_CLEAR_DELAY		90

simple_bt_status simple_status;

union{
	uint16_t value;
	struct {
		uint16_t pairing_on : 1;
		uint16_t power_on : 1;
		uint16_t power_off : 1;
		uint16_t end_call : 1;
		uint16_t default_reset : 1;
		uint16_t clear_delay : 1;
		uint16_t make_call : 1;
	};
} CommandCheck;

static void command_check(void)
{
	if( CommandCheck.value )
	{
		if( CommandCheck.clear_delay )
		{
			bt_simple_clear_delay();
		}
		else if( CommandCheck.power_off )
		{
			if( (simple_status == SIMPLE_BT_ON) || (simple_status == SIMPLE_BT_HFP_CONNECTED) )
			bt_simple_power_off();
		}
		else if( CommandCheck.pairing_on )
		{
			bt_simple_pairing_on();
		}
		else if( CommandCheck.power_on )
		{
			bt_simple_power_on();
		}
		else if( CommandCheck.make_call )
		{
			bt_make_call();
		}
		else if( CommandCheck.default_reset )
		{
			bt_simple_default_reset();
		}
		else if( CommandCheck.end_call )
		{
			bt_simple_power_on();
		}
		
	}
}

void bt_simple_task( void )
{
	BT_CommandDecodeMain();
	BT_CommandSendTask();
	nextCommandReqCheck();
	
	// mfb command check
	command_check();
}

void bt_simple_pairing_on( void )
{
	if( g_str_timer.ui16_mfb_cnt )
	{
		CommandCheck.pairing_on = 1;
	}
	else
	{
		g_str_timer.ui16_mfb_cnt = SIMPLE_PAIRING_ON_DELAY;
		BT_MFB_ENABLE;
		CommandCheck.pairing_on = 0;
		CommandCheck.clear_delay = 1;
		g_str_bitflag.b1_bt_conn = 1;
	}
	
}

void bt_simple_power_on( void )
{
	if( g_str_timer.ui16_mfb_cnt )
	{
		CommandCheck.power_on = 1;
	}
	else
	{
		g_str_timer.ui16_mfb_cnt = SIMPLE_POWER_ON_DELAY;
		BT_MFB_ENABLE;
		CommandCheck.power_on = 0;
		CommandCheck.clear_delay = 1;
		g_str_bitflag.b1_bt_conn = 1;
	}
}

void bt_simple_power_off( void )
{
	if( g_str_timer.ui16_mfb_cnt )
	{
		CommandCheck.power_off = 1;
	}
	else
	{
		g_str_timer.ui16_mfb_cnt = SIMPLE_POWER_OFF_DELAY;
		BT_MFB_ENABLE;
		CommandCheck.power_off = 0;
		CommandCheck.clear_delay = 1;
		g_str_bitflag.b1_bt_conn = 1;
	}
}

void bt_simple_end_call( void )
{
	if( g_str_timer.ui16_mfb_cnt )
	{
		CommandCheck.end_call = 1;
	}
	else
	{
		g_str_timer.ui16_mfb_cnt = SIMPLE_LONG_KEY_DELAY;
		BT_MFB_ENABLE;
		CommandCheck.end_call = 0;
		CommandCheck.clear_delay = 1;
		g_str_bitflag.b1_bt_conn = 1;
	}
	
	if( (simple_status == SIMPLE_BT_ON) || (simple_status == SIMPLE_BT_HFP_CONNECTED) )
	bt_simple_power_off();
}

void bt_simple_clear_delay( void )
{
	if( g_str_timer.ui16_mfb_cnt )
	{
		CommandCheck.clear_delay = 1;
	}
	else
	{
		BT_MFB_DISABLE;
		g_str_timer.ui16_mfb_cnt = SIMPLE_CLEAR_DELAY;
		CommandCheck.clear_delay = 0;
		g_str_bitflag.b1_bt_conn = 1;
	}
}
void bt_simple_default_reset( void )
{
	if( g_str_timer.ui16_mfb_cnt )
	{
		CommandCheck.default_reset = 1;
	}
	else
	{
		g_str_timer.ui16_mfb_cnt = SIMPLE_DEFAULT_RESET_DELAY;
		BT_MFB_ENABLE;
		CommandCheck.default_reset = 0;
		CommandCheck.clear_delay = 1;
		g_str_bitflag.b1_bt_conn = 1;
	}
}

void bt_make_call( void )
{
#ifdef ENABLE_FAVORITE_CALL
	if( simple_status == SIMPLE_BT_OFF )
	{
		bt_simple_power_on();
		simple_status = SIMPLE_BT_STANDBY;
		
		CommandCheck.make_call = 1;
		g_str_timer.ui16_make_call_cnt = SIMPLE_CLEAR_DELAY;
	}
	else
	{
		if( !g_str_timer.ui16_make_call_cnt )
		{
			if( g_device_status == DEVICE_WAIT_EVENT )
			{
				make_favorite_call(g_str_favo.ui16_index);
				CommandCheck.make_call = 0;
			}
		}
		else
		{
			CommandCheck.make_call = 1;
		}
	}
#endif
}

#endif