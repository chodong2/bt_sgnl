#include "global_define.h"

void monitor_init(void)
{
	monitor_timer_config();
	monitor_timer_start();
}

/* Timer Configuration
* Use Timer 4
*/
void monitor_timer_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	/* Enable the TIM4 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = MONITOR_TIM_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	TIM_TimeBaseInitTypeDef TIM_BaseStruct;
	
	TIM_DeInit(MONITOR_TIM_BASE);
	
	/* TIM4 clock enable */
	RCC_APB1PeriphClockCmd(MONITOR_TIM_RCC, ENABLE);
	/* Time base configuration */
	TIM_BaseStruct.TIM_Period = SystemCoreClock / 10000 - 1; // N (us)
	TIM_BaseStruct.TIM_Prescaler = 50; // 50 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(MONITOR_TIM_BASE, &TIM_BaseStruct);
	
	/* TIM IT enable */
	TIM_ITConfig(MONITOR_TIM_BASE, TIM_IT_Update, ENABLE);
}

void monitor_timer_stop(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, DISABLE);
}

void monitor_timer_start(void)
{
	TIM_Cmd(MONITOR_TIM_BASE, ENABLE);
}

void monitor_timer_isr(void)
{
	if (TIM_GetITStatus(MONITOR_TIM_BASE, TIM_IT_Update) != RESET)
	{
#ifdef ENABLE_IS206x
		if ( g_str_bitflag.b1_power_down )
		{
			g_str_timer.ui16_bt_power_off_cnt++;
		}
		else
		{
			g_str_timer.ui16_sleep_cnt++;
		}

#ifdef ENABLE_SIMPLE_BT
		if( (simple_status == SIMPLE_BT_OFF) && (g_str_bitflag.b2_make_call_status == 3) )// Disable NRST pin after end call
		{
			BT_NRST_DISABLE;
			g_str_bitflag.b2_make_call_status = 0;
		}

		if(g_str_bitflag.b1_bt_conn)// when received delay count before bt_on in ble protocol
		{
			if( g_str_timer.ui16_mfb_cnt == 0 )
			{
				BT_MFB_DISABLE;
				g_str_bitflag.b1_bt_conn = 0;
				g_str_bitflag.b1_nrst_flag = 1;
			}
			else
			{
				g_str_timer.ui16_mfb_cnt--;
				g_str_timer.ui16_sleep_cnt = 0;
			}
		}
#endif	//ENABLE_SIMEPLE_BT

		if( ( g_str_bitflag.b3_ble_call_command == CALL_MAKE_CALL ) ||  (g_str_bitflag.b3_ble_call_command == CALL_INCOMING_CALL)
			|| (g_str_bitflag.b3_ble_call_command == CALL_RECEIVE_CALL) || (g_str_bitflag.b3_ble_report == BLE_REPORT_BT_PAIRING_ON) )
		{
			g_str_timer.ui16_sleep_cnt = 0;
#ifdef ENABLE_SIMPLE_BT
			if( simple_status == SIMPLE_BT_HFP_CONNECTED )
#else
			if( ( BT_SystemStatus == BT_SYSTEM_CONNECTED ) && ( BT_LinkbackStatus == BT_LINK_CONNECTED ) )// when HFP connection is successed by power on command
#endif
				g_str_timer.ui16_ble_delay_cnt++;
			
			if(g_str_bitflag.b3_ble_report == BLE_REPORT_INIT)
				g_str_timer.ui16_ble_delay_cnt = 0;

			if(g_str_bitflag.b3_ble_call_command == CALL_INCOMING_CALL)
			{
				if( BTAPP_GetStatus() != BT_STATUS_ON )
				{
					g_str_timer.ui16_bt_wakeup_cnt++;
					if(g_str_timer.ui16_bt_wakeup_cnt >= 500) //
					{
						g_str_timer.ui16_bt_wakeup_cnt = 0;
						BTAPP_TaskReq( BT_REQ_SYSTEM_ON );
					}
				}
			}
		}
		else if( g_device_status == DEVICE_CALLING )
		{
			g_str_timer.ui16_ble_delay_cnt = 0;
			g_str_timer.ui16_sleep_cnt = 0;
			g_str_timer.ui16_bt_power_off_cnt = 0;
			g_str_bitflag.b1_power_down = 0;
		}
#ifdef ENABLE_SIMPLE_BT
		else if(simple_status == SIMPLE_BT_DISCOVERABLE)
#else
		else if( ( g_device_status == DEVICE_PAIRING ) && ( g_str_bitflag.b3_bt_status == PAIRING_START ) )// when BT enter pairing mode
#endif
		{
			if( (g_str_bitflag.b3_ble_report == BLE_REPORT_DEFAULT_RESET) && (!g_str_bitflag.b1_pairing_flag) )
			{
				g_str_timer.ui16_sleep_cnt = 0;
			}
			else
			{
				g_str_timer.ui16_bt_pairing_cnt++;
				if(g_str_timer.ui16_bt_pairing_cnt == PAIRING_COUNT)
				{
					g_str_bitflag.b3_bt_status = PAIRING_TIMEOUT;
					g_str_bt.ui16_bt_mode_count = SLEEP_COUNT;
					g_str_timer.ui16_bt_pairing_cnt = 0;
				}
			}
		}
		
		if( BT_LinkbackStatus == BT_PAIRING_OK )
		{
			g_str_timer.ui16_sleep_cnt = 0;
		}
		

		if(BTAPP_timer1ms)
			--BTAPP_timer1ms;
		
		if(BT_CommandSentMFBWaitTimer)
			--BT_CommandSentMFBWaitTimer;
		
		if( BT_CommandStartMFBWaitTimer)
		{
			--BT_CommandStartMFBWaitTimer;
			if (!BT_CommandStartMFBWaitTimer)
			{
#if 0
				if ( g_str_bitflag.b1_nrst_flag )
				{
					BT_CommandStartMFBWaitTimer++;
				}
				else
#endif
				{
					if (BT_CMD_SendState == BT_CMD_SEND_MFB_HIGH_WAITING)
					{
						BT_CMD_SendState = BT_CMD_SEND_DATA_SENDING;
						UART_TransferFirstByte();
					}
				}
			}
		}
		
#ifdef ENABLE_FREQ_CHECK
		if (g_str_bitflag.b3_freq_chk == FREQ_CHK_START)
		{
			g_str_bitflag.b3_freq_chk = FREQ_CHK_ING;
			g_str_audio.ui16_freq_cnt = 0;
			g_str_audio.ui16_sec_cnt = 0;
		}
		else if (g_str_bitflag.b3_freq_chk == FREQ_CHK_ING)
		{
			g_str_audio.ui16_sec_cnt++;
		}
		else if(g_str_bitflag.b3_freq_chk == FREQ_CHK_END)
		{
			g_str_timer.ui16_audio_cnt = 0;
		}
		
		if (g_str_audio.ui16_sec_cnt == 20)
		{
			g_str_audio.ui16_freq_cnt = 0;
		}
		else if (g_str_audio.ui16_sec_cnt == 30)
		{
			g_str_bitflag.b3_freq_chk = FREQ_CHK_ANALYZE;
			g_str_audio.ui16_sec_cnt = 0;
		}
#endif	//ENABLE_FREQ_CHECK
#endif	//ENABLE_IS206x
		
		TIM_ClearITPendingBit(MONITOR_TIM_BASE, TIM_IT_Update);
	}
}

