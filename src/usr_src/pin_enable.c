#include "global_define.h"

void pin_enable_init(void)
{
	pin_enable_gpio();
#ifdef ENABLE_BT_UART
	pin_enable_bt_gpio();
#endif
#ifdef ENABLE_BLE_UART
	pin_enable_ble_gpio();
#endif
}

/* POWER Configuration
* Power, Amp  PA12, PB14
*/
void pin_enable_gpio(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

#if 0
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = ~(PIN_GPIO_WAKEUP_PIN | BT_UART_TX_PIN | BT_UART_RX_PIN | AUDIO_I2S_TX_WS_PIN | PIN_GPIO_BT_MFB_PIN);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin = ~(PIN_GPIO_BT_NRST_PIN | BLE_UART_RX_PIN | AUDIO_I2S_TX_SD_PIN | BLE_UART_TX_PIN | AUDIO_I2S_RX_WS_PIN
					| AUDIO_I2S_TX_MCK_PIN | AUDIO_I2S_TX_SCK_PIN | AUDIO_I2S_RX_CK_PIN | PIN_GPIO_AMP_STATUS_PIN | AUDIO_I2S_RX_SD_PIN);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
#endif	

	RCC_AHB1PeriphClockCmd(PIN_GPIO_PWR_HOLD_RCC, ENABLE);
	/* Configure PA12 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = PIN_GPIO_PWR_HOLD_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(PIN_GPIO_PWR_HOLD_PORT, &GPIO_InitStructure);
	
#ifdef ENABLE_AUDIO
	RCC_AHB1PeriphClockCmd(PIN_GPIO_AMP_STATUS_RCC, ENABLE);
	
	/* Configure PB1 and PB14 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = PIN_GPIO_AMP_STATUS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(PIN_GPIO_AMP_STATUS_PORT, &GPIO_InitStructure);
	
	AMP_STATUS_DISABLE;
#endif	//ENABLE_AUDIO
	
#ifdef ENABLE_STANDBY
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_AHB1PeriphClockCmd(PIN_GPIO_WAKEUP_FLAG_RCC, ENABLE);

	/* Configure PA0 in input mode */
	GPIO_InitStructure.GPIO_Pin = PIN_GPIO_WAKEUP_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(PIN_GPIO_WAKEUP_FLAG_PORT, &GPIO_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	SYSCFG_EXTILineConfig(PIN_GPIO_WAKEUP_EXTI_PORT, PIN_GPIO_WAKEUP_EXTI_PIN_SOURCE);
	
	/* PA0 is connected to EXTI_Line0 */
	EXTI_InitStructure.EXTI_Line = PIN_GPIO_WAKEUP_EXTI_LINE;
	/* Enable interrupt */
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	/* Interrupt mode */
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* Triggers on rising edge */
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	/* Add to EXTI */
	EXTI_Init(&EXTI_InitStructure);
	
	/* EXTI13~15 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = PIN_GPIO_WAKEUP_EXTI_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif

	RCC_APB1PeriphResetCmd(RCC_APB1Periph_PWR, ENABLE);
	PWR_WakeUpPinCmd(ENABLE);
}

void pin_enable_bt_gpio(void)
{
#ifdef ENABLE_IS206x
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(PIN_GPIO_BT_MFB_RCC, ENABLE);

	GPIO_InitStructure.GPIO_Pin = PIN_GPIO_BT_MFB_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(PIN_GPIO_BT_MFB_PORT, &GPIO_InitStructure);

	RCC_AHB1PeriphClockCmd(PIN_GPIO_BT_NRST_RCC, ENABLE);
	
	/* Configure PA6 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = PIN_GPIO_BT_NRST_PIN;
	GPIO_Init(PIN_GPIO_AMP_STATUS_PORT, &GPIO_InitStructure);
	
	BT_MFB_DISABLE;
#ifdef ENABLE_SIMPLE_BT
	BT_NRST_ENABLE;
#else
	BT_NRST_DISABLE;
#endif
#endif //ENABLE_IS206x
}

#ifdef ENABLE_BLE_UART
void pin_enable_ble_gpio(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(PIN_GPIO_BLE_TX_FLAG_RCC, ENABLE);

	GPIO_InitStructure.GPIO_Pin = PIN_GPIO_BLE_TX_FLAG_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(PIN_GPIO_BLE_TX_FLAG_PORT, &GPIO_InitStructure);
	
	RCC_AHB1PeriphClockCmd(BLE_UART_CTS_RCC, ENABLE);
	/* Configure PA7 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = BLE_UART_CTS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(BLE_UART_CTS_PORT, &GPIO_InitStructure);
	BLE_UART_CTS_DISABLE;

	/* Configure PA8 in input mode */
	RCC_AHB1PeriphClockCmd(BLE_UART_RTS_RCC, ENABLE);
	GPIO_InitStructure.GPIO_Pin = BLE_UART_RTS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(BLE_UART_RTS_PORT, &GPIO_InitStructure);
}
#endif //ENABLE_BLE_UART
