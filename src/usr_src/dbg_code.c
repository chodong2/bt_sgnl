#include "global_define.h"

#ifdef ENABLE_DBG

void dbg_init(void)
{
	dbg_uart_config();
}

/* USART1 Configuration
 * TxD  PA9
 * RxD  PA10
 * async. mode
 * baudrate 115200
 * Word Length = 8 Bits
 * one Stop Bit
 * No parity
 * Hardware flow control disabled (RTS and CTS signals)
 * Receive and transmit enabled
 */
void dbg_uart_config(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(DBG_UART_GPIO_CLK, ENABLE);

	/* Connect USART pins to AF7 */
	GPIO_PinAFConfig(DBG_UART_GPIO_PORT, DBG_UART_TX_SOURCE, DBG_UART_AF);
	GPIO_PinAFConfig(DBG_UART_GPIO_PORT, DBG_UART_RX_SOURCE, DBG_UART_AF);

	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

	GPIO_InitStructure.GPIO_Pin = DBG_UART_TX_PIN | DBG_UART_RX_PIN;
	GPIO_Init(DBG_UART_GPIO_PORT, &GPIO_InitStructure);


	/* Enable USART clock */
	RCC_APB2PeriphClockCmd(DBG_UART_CLK, ENABLE);


	/* USART1 configuration ----------------------------------------------------*/
	USART_InitStructure.USART_BaudRate = DBG_UART_BAUDRATE;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(DBG_UART, &USART_InitStructure);

	/* NVIC configuration */

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DBG_UART_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable USART */
	USART_Cmd(DBG_UART, ENABLE);
	USART_ITConfig(DBG_UART, USART_IT_RXNE, ENABLE);

}

#if 0
void dbg_print( uint8_t byte[] )
{
	uint16_t ui16_index = 0;

	//while( g_str_dbg.ui16_tx_cnt != 0 );

	g_str_dbg.ui8_tx_buf[ui16_index++] = '\r';
	g_str_dbg.ui8_tx_buf[ui16_index++] = '\n';

	for( int i = 0; i < UART_BUFFERSIZE; i++ )
	{
		if( byte[i] == '\n' )
		{
			g_str_dbg.ui8_tx_buf[ui16_index++] = '\r';
			g_str_dbg.ui8_tx_buf[ui16_index++] = byte[i];

			if( byte[i+1] == '\0' )
			{
				g_str_dbg.ui16_tx_size = ui16_index;
				break;
			}
		}
		g_str_dbg.ui8_tx_buf[ui16_index++] = byte[i];
	}

	USART_ITConfig(DBG_UART, USART_IT_TXE, ENABLE);
}
#else
void dbg_print( const char *form, ... )
{
        static char buf[256] = {0,};
	uint16_t ui16_index = 0;
        uint16_t len = 0;
        va_list arg_ptr;
        
        len = strlen(form);
        if(len > 0)
        {
                memset(buf, 0x00, sizeof(buf));
                va_start(arg_ptr, form);
                vsprintf(buf, form, arg_ptr);
                va_end(arg_ptr);

                for( int i = 0; i < UART_BUFFERSIZE; i++ )
                {
                        if( buf[i] == '\n' )
                        {
                                g_str_dbg.ui8_tx_buf[ui16_index++] = '\r';
                                g_str_dbg.ui8_tx_buf[ui16_index++] = buf[i];

                                if( buf[i+1] == '\0' )
                                {
                                        g_str_dbg.ui16_tx_size = ui16_index;
                                        break;
                                }
                        }
                        g_str_dbg.ui8_tx_buf[ui16_index++] = buf[i];
                }
                
                USART_ITConfig(DBG_UART, USART_IT_TXE, ENABLE);
        }
}
#endif

void dbg_menu( void )
{
	AMP_STATUS_DISABLE;
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);

	dbg_print("\r 1. led test \n\
                \r 2. btn test \n\
                \r 3. audio test \n\
                \r 4. haptic test \n\
                \r 5. pedometer test \n\
                \r 6. aging test \n\
                \r ============= \n\
                \r 9. exit \n");
}

void dbg_uart_isr(void)
{
#ifdef ENABLE_LOWPWR
	g_str_bitflag.b1_power_down = 0;
	g_str_timer.ui16_sleep_cnt = 0;
#endif

	if( USART_GetITStatus( DBG_UART, USART_IT_TXE) == SET )
	{
		USART_SendData( DBG_UART, g_str_dbg.ui8_tx_buf[g_str_dbg.ui16_tx_cnt++] );

		if( g_str_dbg.ui16_tx_cnt == g_str_dbg.ui16_tx_size )
		{
			/* Disable the Tx buffer empty interrupt */
			USART_ITConfig(DBG_UART, USART_IT_TXE, DISABLE);

			g_str_dbg.ui16_tx_cnt = 0;
			g_str_dbg.ui16_tx_size = 0;
		}

		USART_ClearITPendingBit(DBG_UART, USART_IT_TXE);
	}

	/* USART in Receiver mode */
	if (USART_GetITStatus(DBG_UART, USART_IT_RXNE) == SET)
	{
		/* Receive Transaction data */

		g_str_dbg.ui8_rx_buf[g_str_dbg.ui16_rx_cnt] = USART_ReceiveData(DBG_UART);

		if( g_str_dbg.ui8_rx_buf[g_str_dbg.ui16_rx_cnt] == '\r' )
		{
			g_str_bitflag.b1_dbg_parsing = 1;
			dbg_print("\n");
		}
		else
		{
			USART_SendData( DBG_UART, g_str_dbg.ui8_rx_buf[g_str_dbg.ui16_rx_cnt] );
		}

		if( g_str_dbg.ui8_rx_buf[g_str_dbg.ui16_rx_cnt] == 0x7f )
		{
			g_str_dbg.ui8_rx_buf[g_str_dbg.ui16_rx_cnt] = '\0';
			g_str_dbg.ui8_rx_buf[g_str_dbg.ui16_rx_cnt-1] = '\0';

			if( g_str_dbg.ui16_rx_cnt > 0 )
			{
				g_str_dbg.ui16_rx_cnt--;
			}
		}
		else
		{
			g_str_dbg.ui16_rx_cnt++;
		}

		if( g_str_dbg.ui16_rx_cnt == UART_BUFFERSIZE )
		{
			g_str_dbg.ui16_rx_cnt = 0;
		}

		USART_ClearITPendingBit(DBG_UART, USART_IT_RXNE);
	}
}

void dbg_parser( void )
{
	if( g_str_dbg.ui8_rx_buf[0] == 'h' && 
			g_str_dbg.ui8_rx_buf[1] == 'e' &&
			g_str_dbg.ui8_rx_buf[2] == 'l' &&
			g_str_dbg.ui8_rx_buf[3] == 'p' &&
			g_str_dbg.ui8_rx_buf[4] == '\r' )
	{
		dbg_menu();

		g_str_bitflag.b1_dbg_enable = 1;
	}

	else if( g_str_bitflag.b1_dbg_enable )
	{
		if( g_str_dbg.ui8_rx_buf[1] == '\r' )
		{
			switch( g_str_dbg.ui8_rx_buf[0] )
			{
			case '1' :
				dbg_print("debug led mode\n");

				//bt power on ok
				g_str_bitflag.b3_dbg_mode = 1;
				break;
			case '2' :
				dbg_print("debug btn mode\n");
				g_str_bitflag.b3_dbg_mode = 2;
				break;
			case '3' :
				dbg_print("debug audio mode\n");
				g_str_bitflag.b3_dbg_mode = 3;
				g_ui16_tx_idx = 0;
				I2S_Cmd(AUDIO_I2S_TX, ENABLE);
				AMP_STATUS_ENABLE;
				break;
			case '4' :
				dbg_print("debug haptic mode\n");
				g_str_bitflag.b3_dbg_mode = 4;
				break;
			case '5' :
				dbg_print("debug pedometer mode\n");
				g_str_bitflag.b3_dbg_mode = 5;
				break;
			case '9' :
				dbg_print("exit debug mode\n");
				g_str_bitflag.b1_dbg_enable = 0;
				g_str_bitflag.b3_dbg_mode = 0;
				break;
			default :
				dbg_menu();

			}
		}
		else
		{
			dbg_menu();
		}

	}

	for( int i = 0; i < g_str_dbg.ui16_rx_cnt ; i++ )
	{
		g_str_dbg.ui8_rx_buf[i] = '\0';
	}

	g_str_bitflag.b1_dbg_parsing = 0;
	g_str_dbg.ui16_rx_cnt = 0;
}

#endif //ENABLE_DBG


