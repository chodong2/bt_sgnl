#include "global_define.h"

#ifdef ENABLE_BLE_UART

typedef enum {
	RX_BLE_CMD_INIT,
	RX_BLE_CMD_LENGTH,
	RX_BLE_CMD_DATA,
	RX_BLE_CMD_DATA2,
	RX_BLE_CMD_END,
} RX_BLE_MODE;

static RX_BLE_MODE BLE_CmdDecode;

uint8_t  BLE_CmdBuffer[20];
uint16_t BLE_CmdIndex = 0;


static uint8_t ui8_tx_byte[20];
static uint16_t ui16_tx_length;

void ble_uart_init(void)
{
	ble_uart_config();
}

/* UART Configuration
* TxD  PA2
* RxD  PA3
* async. mode
* baudrate 115200
* Word Length = 8 Bits
* one Stop Bit
* No parity
* Hardware flow control disabled (RTS and CTS signals)
* Receive and transmit enabled
*/
void ble_uart_config(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	USART_DeInit(BLE_UART);	
	
	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(BLE_UART_GPIO_CLK, ENABLE);
	
	/* Enable USART clock */
	RCC_APB2PeriphClockCmd(BLE_UART_CLK, ENABLE);
	
	/* Connect USART pins to AF7 */
	//GPIO_PinAFConfig(BLE_UART_GPIO_PORT, BLE_UART_CTS_SOURCE, BLE_UART_AF);
	//GPIO_PinAFConfig(BLE_UART_GPIO_PORT, BLE_UART_RTS_SOURCE, BLE_UART_AF);
	
	GPIO_PinAFConfig(BLE_UART_GPIO_PORT, BLE_UART_TX_SOURCE, BLE_UART_AF);
	GPIO_PinAFConfig(BLE_UART_GPIO_PORT, BLE_UART_RX_SOURCE, BLE_UART_AF);
	
	/* Configure USART Tx and Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	
	GPIO_InitStructure.GPIO_Pin = BLE_UART_TX_PIN;
	GPIO_Init(BLE_UART_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = BLE_UART_RX_PIN;
	GPIO_Init(BLE_UART_GPIO_PORT, &GPIO_InitStructure);
		
	//GPIO_InitStructure.GPIO_Pin = BLE_UART_CTS_PIN;
	//GPIO_Init(BLE_UART_GPIO_PORT, &GPIO_InitStructure);
	
	//GPIO_InitStructure.GPIO_Pin = BLE_UART_RTS_PIN;
	//GPIO_Init(BLE_UART_GPIO_PORT, &GPIO_InitStructure);
	
	
	/* USART2 configuration ----------------------------------------------------*/
	USART_InitStructure.USART_BaudRate = BLE_UART_BAUDRATE;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//USART_HardwareFlowControl_RTS_CTS;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(BLE_UART, &USART_InitStructure);
	
	/* NVIC configuration */
	
	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = BLE_UART_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Enable USART */
	USART_Cmd(BLE_UART, ENABLE);
	USART_ITConfig(BLE_UART, USART_IT_RXNE, ENABLE);
}

void ble_uart_isr(void)
{
	
#ifdef ENABLE_STANDBY
	g_str_timer.ui16_sleep_cnt = 0;
#endif
	
	/* USART in Receiver mode */
	if (USART_GetITStatus(BLE_UART, USART_IT_RXNE) == SET)
	{
		/* Receive Transaction data */
		g_str_ble.ui8_rx_buf[g_str_ble.ui16_rx_head++] = USART_ReceiveData(BLE_UART);

		if( g_str_ble.ui16_rx_head == BLE_UART_BUFFERSIZE )
		{
			g_str_ble.ui16_rx_head = 0;
		}
		
		g_str_ble.ui16_rx_cnt++;
		
		USART_ClearITPendingBit(BLE_UART, USART_IT_RXNE);
	}
	
}

uint8_t ble_uart_read_byte( void )
{
	uint8_t ui8_data = 0;
	
	ui8_data = g_str_ble.ui8_rx_buf[g_str_ble.ui16_rx_tail++];
	
	if( g_str_ble.ui16_rx_tail == BLE_UART_BUFFERSIZE )
	{
		g_str_ble.ui16_rx_tail = 0;
	}
	g_str_ble.ui16_rx_cnt--;
	
	return ui8_data;
}

void BLE_CommandHandler( void )
{
	uint8_t current_byte;
	while (g_str_ble.ui16_rx_cnt)
	{
		current_byte = ble_uart_read_byte();
		if ( (current_byte == 0xFA) && ( BLE_CmdDecode == RX_BLE_CMD_DATA ) )
		{
			BLE_CmdDecode = RX_BLE_CMD_END;
		}
		
		switch (BLE_CmdDecode) {
		case RX_BLE_CMD_INIT:
			if (current_byte == '$')
				BLE_CmdDecode = RX_BLE_CMD_LENGTH;
			break;
		case RX_BLE_CMD_LENGTH :
			BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
			BLE_CmdDecode = RX_BLE_CMD_DATA;
			break;
		case RX_BLE_CMD_DATA :
			BLE_CmdBuffer[BLE_CmdIndex++] = current_byte;
			break;
		case RX_BLE_CMD_END :
			if (current_byte == 0xFA)
			{
				//ok
				ble_protocol(BLE_CmdBuffer, BLE_CmdBuffer[0]);
				BLE_CmdIndex = 0;
				memset(BLE_CmdBuffer, 0x00, sizeof(BLE_CmdBuffer));
			}
			BLE_CmdDecode = RX_BLE_CMD_INIT;
			break;
		default :
			BLE_CmdDecode = RX_BLE_CMD_INIT;
			BLE_CmdIndex = 0;
			break;
		}
	}
}

void ble_uart_send_data( uint8_t byte )
{
	USART_SendData(BLE_UART, byte);
	
	while (USART_GetFlagStatus(BLE_UART, USART_FLAG_TXE) == RESET)
	{}
}

void ble_uart_send( uint8_t* ui8_byte, uint16_t ui16_length )
{
	for( int i = 0; i < ui16_length; i++ )
	{
		ui8_tx_byte[i] = ui8_byte[i];
		ble_uart_send_data(ui8_tx_byte[i]);
	}

	ble_uart_send_data(0xFA);
}

void ble_command_send_data( uint8_t command, uint8_t param )
{
	uint8_t byte[20] = {0,};
	sprintf((char *)byte, "%c%c%c%c", 0x03, command, param, 0xFA);
	ble_uart_send((uint8_t *)byte, 4);
}

void ble_lowpower_uart_tx( void )
{
	for( int i = 0; i < ui16_tx_length; i++ )
	{
		ble_uart_send_data(ui8_tx_byte[i]);
	}
	ble_uart_send_data(0xFA);
	
	g_str_ble.ui16_tx_flag = 0;
}

void ble_lowpower_uart_send( uint8_t* ui8_byte, uint16_t ui16_length )
{
	if( !g_str_ble.ui16_tx_flag )
	{
		g_str_ble.ui16_tx_flag = 1;
		
		for( int i = 0; i < ui16_length; i++ )
		{
			ui8_tx_byte[i] = ui8_byte[i];
		}
		ui16_tx_length = ui16_length;
	}
}

#endif //ENABLE_BLE_UART
