#include "global_define.h"

#ifdef ENABLE_DATA_LOG

uint8_t load_logcnt_flash( void )
{
	uint32_t ui32_last_cnt = 0;
	
	for( int i = FLASH_ADDR_DATA_LOG + 4; i < FLASH_ADDR_START_APP1; i += 8 )
	{
		ui32_last_cnt = *(__IO uint32_t*) (i);
		
		if( ( ui32_last_cnt & 0x00FF ) == 0xFF )
		{
			break;
		}
		else
		{
			g_str_log.ui16_log_cnt = (uint16_t)( ui32_last_cnt & 0xFFFF );
		}
	}
	
	return 1;
}

uint8_t write_log_flash( datalog_t ui8_log )
{
	uint32_t ui32_address = FLASH_ADDR_DATA_LOG + (g_str_log.ui16_log_cnt++<<3);
	
	g_str_log.ui32_log = g_str_log.ui16_log_cnt | ui8_log<<16;
	
	if( ui32_address >= FLASH_ADDR_DATA_LOG_END )
	{
		//data full
		return 0;
	}
	
	

	FLASH_Unlock();
	
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);
				
	if (FLASH_ProgramWord(ui32_address, g_str_log.ui32_date ) == FLASH_COMPLETE)
	{
		if (FLASH_ProgramWord(ui32_address+4, g_str_log.ui32_log ) == FLASH_COMPLETE)
		{
			FLASH_Lock();
			return 1;
		}
	}
	else
	{ 
		/* Error occurred while writing data in Flash memory. 
		User can add here some code to deal with this error */
		FLASH_Lock();
		return 0;// FLASH_ERROR_WRITE;
	}
	
	return 1;
}

uint8_t reset_log_flash( void )
{
	/* Unlock the Flash to enable the flash control register access *************/ 
	FLASH_Unlock();
	
	/* Erase the user Flash area
	(area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/
	
	/* Clear pending flags (if any) */  
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
			FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR); 
	
	/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
	be done by word */
	for (int i = ERASE_LOG_START_SECTOR; i < ERASE_LOG_END_SECTOR; i += 8)
	{
		/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
		be done by word */ 
		if (FLASH_EraseSector(i, VoltageRange_3) != FLASH_COMPLETE)
		{ 
			/* Error occurred while sector erase. 
			User can add here some code to deal with this error  */
			return 0;
		}
	}
	
	if(FLASH_ProgramWord(FLASH_ADDR_JUMP_APP, FLASH_ADDR_CURRENT_APP)
	   != FLASH_COMPLETE)
	{
		//ret = FLASH_ERROR_WRITE;
	}
	/* Lock the Flash to disable the flash control register access (recommended
	to protect the FLASH memory against possible unwanted operation) *********/
	FLASH_Lock();
	
	g_str_log.ui16_log_cnt = 0;
	
	return 1;
}

uint8_t print_log_flash( void )
{
	
#ifdef ENABLE_LOG_FILE
	uint32_t ui32_last_data = 0, ui32_last_data2 = 0;
	
	for( int i = FLASH_ADDR_DATA_LOG; i < FLASH_ADDR_START_APP1; i += 8 )
	{
		ui32_last_data = *(__IO uint32_t*) (i);
		ui32_last_data2 = *(__IO uint32_t*) (i+4);
		
		if( ( ui32_last_data & 0x00FF ) == 0xFF )
		{
			break;
		}
		else
		{
			uint8_t year	= ui32_last_data & 0x00FF;
			uint8_t month	= (ui32_last_data>>8) & 0x00FF;
			uint8_t day		= (ui32_last_data>>16) & 0x00FF;
			uint8_t hour	= (ui32_last_data>>24) & 0x00FF;
			uint8_t log		= (ui32_last_data2>>16) & 0x00FF;
			uint16_t logcnt	= ui32_last_data2 & 0x00FFFF;
			
			printf("date:%d-%d-%d %dh, ", year,month,day,hour);
			
			switch( log )
			{
			case LOG_POWER_ON :				printf("POWER_ON ");
				
				break;
			case LOG_POWER_OFF_USER :		printf("POWER_OFF_USER ");
				break;
			case LOG_POWER_OFF_BATT :		printf("POWER_OFF_BATT ");
				break;

			case LOG_INITIALIZE_OK :		printf("INITIALIZE_OK ");
				break;
			case LOG_POWER_ON_RESET :		printf("POWER_ON_RESET ");
				break;
			case LOG_WATCHDOG_RESET :		printf("WATCHDOG_RESET ");
				break;

			case LOG_CHARGER_ENABLE :		printf("CHARGER_ENABLE ");
				break;
			case LOG_CHARGER_DISABLE :		printf("CHARGER_DISABLE ");
				break;
			case LOG_CHARGE_COMPLETE :		printf("CHARGE_COMPLETE ");
				break;
			case LOG_CHARGE_FAIL:
				break;
			case LOG_LOWBATT_ALARM :		printf("LOWBATT_ALARM ");
				break;

			case LOG_ENTER_PAIRING :		printf("ENTER_PAIRING ");
				break;
			case LOG_PAIRING_SUCCESS :		printf("PAIRING_SUCCESS ");
				break;
			case LOG_PAIRING_FAIL :			printf("PAIRING_FAIL ");
				break;

			case LOG_HFP_CONNECTED :		printf("HFP_CONNECTED ");
				break;
			case LOG_HFP_DISCONNECTED :		printf("HFP_DISCONNECTED ");
				break;

			case LOG_BLE_ADVERTISING :		printf("BLE_ADVERTISING ");
				break;
			case LOG_BLE_CONNECTED :		printf("BLE_CONNECTED ");
				break;
			case LOG_BLE_DISCONNECTED :		printf("BLE_DISCONNECTED ");
				break;
			case LOG_BLE_ANCS_FOUND :		printf("BLE_ANCS_FOUND ");
				break;

			case LOG_CALL_STATUS_INCOM :	printf("CALL_STATUS_INCOM ");
				break;
			case LOG_CALL_STATUS_OUTGO :	printf("CALL_STATUS_OUTGO ");
				break;
			case LOG_CALL_STATUS_CALLING :	printf("CALL_STATUS_CALLING ");
				break;
			case LOG_CALL_STATUS_REJECT :	printf("CALL_STATUS_REJECT ");
				break;
			case LOG_CALL_STATUS_END_CALL :	printf("CALL_STATUS_END_CALL ");
				break;

			case LOG_DO_NOT_DISTURB_ON :	printf("DO_NOT_DISTURB_ON ");
				break;
			case LOG_DO_NOT_DISTURB_OFF :	printf("DO_NOT_DISTURB_OFF ");
				break;

			case LOG_LED_FEEDBACK_ON :		printf("LED_FEEDBACK_ON ");
				break;
			case LOG_LED_FEEDBACK_OFF :		printf("LED_FEEDBACK_OFF ");
				break;

			case LOG_HAPTIC_FEEDBACK_ON :	printf("HAPTIC_FEEDBACK_ON ");
				break;
			case LOG_HAPTIC_FEEDBACK_OFF :	printf("HAPTIC_FEEDBACK_OFF ");
				break;

			case LOG_TIME_SYNC :			printf("LOG_TIME_SYNC ");
				break;
				
			default : break;
				
				
			}
			printf("cnt:%d\n", logcnt);
		}
	}
#endif //ENABLE_LOG_FILE
	return 1;
}

#endif //ENABLE_DATA_LOG
