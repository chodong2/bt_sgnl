#include "global_define.h"

#ifdef ENABLE_AUDIO

#ifdef ENABLE_VOL_SHIFT
#define VOL_INDEX	6

const int32_t i32_vol_maxmin[VOL_INDEX] = {0,-8192,-16384,0,16383, 8191};
const int32_t i32_vol_vol_shift[VOL_INDEX] =  {0,2,1,0,1,2};
#endif //ENABLE_VOL_SHIFT

void audio_init(uint16_t audio)
{
	audio_i2s_config_rx(audio);
	audio_i2s_config_tx(audio);
}

void audio_i2s_config_rx(uint16_t audio)
{
	I2S_InitTypeDef I2S_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	uint16_t freq = audio;
	
	/* Enable GPIO clocks */
	RCC_APB1PeriphClockCmd(AUDIO_I2S_RX_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(AUDIO_I2S_RX_GPIO_CLOCK, ENABLE);
	
	/* I2S GPIO Configuration --------------------------------------------------*/
	/* Connect I2S pins to Alternate functions */
	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_RX_WS_PIN | AUDIO_I2S_RX_CK_PIN | AUDIO_I2S_RX_SD_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(AUDIO_I2S_RX_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_PinAFConfig(AUDIO_I2S_RX_GPIO_PORT, AUDIO_I2S_RX_WS_SOURCE, AUDIO_I2S_RX_GPIO_AF);
	GPIO_PinAFConfig(AUDIO_I2S_RX_GPIO_PORT, AUDIO_I2S_RX_CK_SOURCE, AUDIO_I2S_RX_GPIO_AF);
	GPIO_PinAFConfig(AUDIO_I2S_RX_GPIO_PORT, AUDIO_I2S_RX_SD_SOURCE, AUDIO_I2S_RX_GPIO_AF);
	
	/* I2S configuration -------------------------------------------------------*/
	SPI_I2S_DeInit(AUDIO_I2S_RX);
	/* Configure the Audio Frequency, Standard and the data format */
	I2S_InitStructure.I2S_Standard = I2S_Standard_Phillips;
	I2S_InitStructure.I2S_DataFormat = I2S_DataFormat_16bextended;
	I2S_InitStructure.I2S_MCLKOutput = I2S_MCLKOutput_Disable;
	
	switch(freq)
	{
	case AUDIO_INPUT_8K:    I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_8k;     break;
	case AUDIO_INPUT_16K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_16k;    break;
	case AUDIO_INPUT_44_1K: I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_44k;    break;
	case AUDIO_INPUT_48K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_48k;    break;
	}
	
	I2S_InitStructure.I2S_CPOL = I2S_CPOL_Low;
	I2S_InitStructure.I2S_Mode = I2S_Mode_SlaveRx;
	I2S_Init(AUDIO_I2S_RX, &I2S_InitStructure);
	
	/* SPI2 IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_I2S_RX_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_RXNE, ENABLE);
	SPI_I2S_ITConfig(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE, ENABLE);
}

void audio_i2s_config_tx(uint16_t audio)
{
	I2S_InitTypeDef I2S_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	uint16_t freq = audio;
	
	/* Enable GPIO clocks */
	RCC_AHB1PeriphClockCmd(AUDIO_I2S_TX_GPIO_CLOCK, ENABLE);
	RCC_APB1PeriphClockCmd(AUDIO_I2S_TX_CLK, ENABLE);
	
	/* I2S GPIO Configuration --------------------------------------------------*/
	/* Connect I2S pins to Alternate functions */
	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_TX_MCK_PIN | AUDIO_I2S_TX_SD_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	
	GPIO_PinAFConfig(AUDIO_I2S_TX_GPIO, AUDIO_I2S_TX_MCK_SOURCE, AUDIO_I2S_TX_GPIO_AF);
	GPIO_PinAFConfig(AUDIO_I2S_TX_GPIO, AUDIO_I2S_TX_SD_SOURCE, AUDIO_I2S_TX_GPIO_AF);
	GPIO_Init(AUDIO_I2S_TX_GPIO, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_TX_SCK_PIN;
	GPIO_PinAFConfig(AUDIO_I2S_TX_GPIO, AUDIO_I2S_TX_SCK_SOURCE, AUDIO_I2S_TX_SCK_GPIO_AF);
	GPIO_Init(AUDIO_I2S_TX_GPIO, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = AUDIO_I2S_TX_WS_PIN;
	GPIO_PinAFConfig(AUDIO_I2S_TX_WS_GPIO, AUDIO_I2S_TX_WS_SOURCE, AUDIO_I2S_TX_GPIO_AF);
	GPIO_Init(AUDIO_I2S_TX_WS_GPIO, &GPIO_InitStructure);
	
	/* I2S configuration -------------------------------------------------------*/
	
	/* Initialize  I2S3 peripherals */
	SPI_I2S_DeInit(AUDIO_I2S_TX);
	/* Configure the Audio Frequency, Standard and the data format */
	I2S_InitStructure.I2S_Standard = I2S_Standard_Phillips;
	I2S_InitStructure.I2S_DataFormat = I2S_DataFormat_16b;
	I2S_InitStructure.I2S_MCLKOutput = I2S_MCLKOutput_Enable;
	
	switch(freq)
	{
	case AUDIO_INPUT_8K:    I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_8k;     break;
	case AUDIO_INPUT_16K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_16k;    break;
	case AUDIO_INPUT_44_1K: I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_44k;    break;
	case AUDIO_INPUT_48K:   I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_48k;    break;
	}
	
	I2S_InitStructure.I2S_CPOL = I2S_CPOL_Low;
	I2S_InitStructure.I2S_Mode = I2S_Mode_MasterTx;
	
	I2S_Init(AUDIO_I2S_TX, &I2S_InitStructure);
	
#ifdef ENABLE_AUDIO_DMA
	Audio_DMA_config();
#else
	NVIC_InitTypeDef NVIC_InitStructure;
	/* SPI IRQ Channel configuration */
	NVIC_InitStructure.NVIC_IRQChannel = AUDIO_I2S_TX_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	SPI_I2S_ITConfig(AUDIO_I2S_TX, SPI_I2S_IT_TXE, ENABLE);
#endif
}

void audio_freq_check(void)
{
	//input audio frequency check
	if (g_str_bitflag.b3_freq_chk == FREQ_CHK_ANALYZE)
	{
		if (g_str_audio.ui16_freq_cnt < 800)
		{
			g_str_audio.ui16_audio_type = AUDIO_INPUT_NONE;
		}
		else if (g_str_audio.ui16_freq_cnt < 1600)
		{
			g_str_audio.ui16_audio_type = AUDIO_INPUT_8K;
		}
		else if (g_str_audio.ui16_freq_cnt < 4410)
		{
			g_str_audio.ui16_audio_type = AUDIO_INPUT_16K;
		}
		else if (g_str_audio.ui16_freq_cnt < 4800)
		{
			g_str_audio.ui16_audio_type = AUDIO_INPUT_44_1K;
		}
		else
		{
			g_str_audio.ui16_audio_type = AUDIO_INPUT_48K;
		}
		
		g_str_audio.ui16_freq_cnt = 0;
		g_str_bitflag.b3_freq_chk = FREQ_CHK_END;
		
		audio_i2s_config_rx(g_str_audio.ui16_audio_type);
		audio_i2s_config_tx(g_str_audio.ui16_audio_type);
		audio_rx_start();
	}
}

void audio_rx_start(void)
{
	I2S_Cmd(AUDIO_I2S_RX, ENABLE);
#ifdef ENABLE_FE
	fe_init();
#endif
}

void audio_rx_stop(void)
{
	I2S_Cmd(AUDIO_I2S_RX, DISABLE);
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);
	g_str_bitflag.b1_audio_rx_state = AUDIO_STATE_STOP;
	g_str_bitflag.b1_audio_tx_state = AUDIO_STATE_STOP;
	AMP_STATUS_DISABLE;
#ifdef ENABLE_AUDIO_DMA
	Audio_MAL_Stop();
#else
	I2S_Cmd(AUDIO_I2S_TX, DISABLE);
#endif
#ifdef ENABLE_FE
	g_str_bitflag.b2_fe_start_flag = 0;
#endif
}

void audio_rx_isr(void)
{
	static uint16_t ui16_i2s_rx_buf[AUDIO_FRAME_SIZE] = { 0, };
	
#ifdef ENABLE_STANDBY
	g_str_bitflag.b1_power_down = 0;
	g_str_timer.ui16_sleep_cnt = 0;
#endif
	
	if (SPI_I2S_GetITStatus(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE) != RESET)
	{
		SPI_I2S_ClearITPendingBit(AUDIO_I2S_RX, SPI_I2S_IT_TIFRFE);
		audio_rx_stop();
		g_str_bitflag.b1_frame_state = AUDIO_FRAME_ERR;
		g_str_timer.ui16_audio_cnt = 0;
	}
	
	/* rx I2S interrupt */
	else if (SPI_I2S_GetITStatus(AUDIO_I2S_RX, SPI_I2S_IT_RXNE) != RESET)
	{
#ifdef ENABLE_FREQ_CHECK
		switch(g_str_bitflag.b3_freq_chk)
		{
		case FREQ_CHK_READY:
			g_str_bitflag.b3_freq_chk = FREQ_CHK_START;
			break;
			
		case FREQ_CHK_ING:
			g_str_audio.ui16_freq_cnt++;
			break;
			
		default:
			break;
		}
#endif
		
		SPI_I2S_ClearITPendingBit(AUDIO_I2S_RX, SPI_I2S_IT_RXNE);
		ui16_i2s_rx_buf[g_ui16_rx_idx] = SPI_I2S_ReceiveData(AUDIO_I2S_RX);
		
#ifdef ENABLE_FE
		g_str_audio.ui16_save_buf[g_str_audio.ui16_rx_frame_no % FE_BUFFER][g_str_audio.ui16_save_cnt++] = ui16_i2s_rx_buf[g_ui16_rx_idx];
#else
		g_str_audio.ui16_save_buf[g_str_audio.ui16_rx_frame_no % AUDIO_FRAME_BUFFER][g_str_audio.ui16_save_cnt++] = ui16_i2s_rx_buf[g_ui16_rx_idx];
#endif
		g_ui16_rx_idx++;
		
		if (g_str_audio.ui16_save_cnt >= AUDIO_FRAME_SIZE)
		{
			
			g_str_audio.ui16_save_cnt = 0;
			g_ui16_rx_idx = 0;
			g_ui16_tx_idx = 0;
			g_str_bitflag.b1_frame_state = AUDIO_FRAME_OK;
			g_str_timer.ui16_audio_cnt = 0;
			
			g_str_audio.ui16_rx_frame_no++;
#ifdef ENABLE_FE
			if ( g_str_bitflag.b2_fe_start_flag == 0 )
			{
				g_str_bitflag.b2_fe_start_flag = 1;
			}
#else
			if( g_str_bitflag.b1_audio_rx_state != AUDIO_STATE_PLAY )
			{
#ifdef ENABLE_AUDIO_DMA
#ifndef ENABLE_FE
				Audio_MAL_Play(g_str_audio.ui16_save_buf,AUDIO_FRAME_SIZE*AUDIO_FRAME_BUFFER);
#endif
#else		
				I2S_Cmd(AUDIO_I2S_TX, ENABLE);
#endif
				g_str_bitflag.b1_audio_rx_state = AUDIO_STATE_PLAY;
			}
			AMP_STATUS_ENABLE;
#endif
		}
	}
}

uint16_t ui16_output = 0;
void audio_tx_isr(void)
{
	
#ifdef ENABLE_STANDBY
	g_str_bitflag.b1_power_down = 0;
	g_str_timer.ui16_sleep_cnt = 0;
#endif
	
	if (SPI_I2S_GetITStatus(AUDIO_I2S_TX, SPI_I2S_IT_TXE) != RESET)
	{
		SPI_I2S_ClearITPendingBit(AUDIO_I2S_TX, SPI_I2S_IT_TXE);  
#if 0
		if( g_str_audio.ui16_audio_type <= AUDIO_INPUT_16K )
		{
			SPI_I2S_SendData(AUDIO_I2S_TX, p_buffer[g_ui16_tx_idx++]);
			if (g_ui16_tx_idx >= AUDIO_FRAME_SIZE*2)
			{
				g_ui16_tx_idx = 0;
#ifdef ENABLE_FE
				if (g_str_fe.ui16_frame_no % 2)
				{
					p_buffer = g_str_fe.ui16_data;
				}
				else
				{
					p_buffer = g_str_fe.ui16_data2;
				}
#endif //ENABLE_FE
				
			}
		}
		else if( g_str_audio.ui16_audio_type >= AUDIO_INPUT_44_1K )
#endif
		{
#ifdef ENABLE_VOL_SHIFT
#ifndef FE_ENABLE
			ui16_output = g_str_audio.ui16_save_buf[g_str_audio.ui16_tx_frame_no%AUDIO_FRAME_BUFFER][g_ui16_tx_idx];
#endif
			
			// vol_level 은 0~5
			if( g_str_audio.ui16_vol_level )
			{
				if( ui16_output & 0x8000 ) // 부호비트 1 음수
				{
					if( g_str_audio.ui16_vol_level > 3 ) // left shift
					{
						if( ui16_output < i32_vol_maxmin[VOL_INDEX - g_str_audio.ui16_vol_level] )
						{
							ui16_output = 0x8000;
						}
						else
						{
							ui16_output <<= i32_vol_vol_shift[g_str_audio.ui16_vol_level];
							ui16_output |= 0x8000; // 부호비트 유지
						}
					}
					else // right shift
					{
						ui16_output >>= i32_vol_vol_shift[g_str_audio.ui16_vol_level];
						
						if(g_str_audio.ui16_vol_level == 1) //1110 0000 0000 0000 //2 shift 1 채우기
						{
							ui16_output |= 0xE000;
						}
						else if( g_str_audio.ui16_vol_level == 2 ) //1100 0000 0000 0000 //1 shift 1 채우기
						{
							ui16_output |= 0xC000;
						}
					}
				}
				else // 부호비트 0 양수
				{
					if( g_str_audio.ui16_vol_level > 3 ) // left shift
					{
						if( ui16_output > i32_vol_maxmin[g_str_audio.ui16_vol_level] )
						{
							ui16_output = 0x7FFF;
						}
						else
						{
							ui16_output <<= i32_vol_vol_shift[g_str_audio.ui16_vol_level];
						}
					}
					else // right shift
					{
						ui16_output >>= i32_vol_vol_shift[g_str_audio.ui16_vol_level];
					}
				}
			}
			else
			{
				ui16_output = 0;
			}
#endif
			
#ifdef ENABLE_FE
			SPI_I2S_SendData(AUDIO_I2S_TX, g_str_fe.ui16_data[g_str_fe.ui16_frame_no % FE_BUFFER][g_ui16_tx_idx++]);
#else
			SPI_I2S_SendData(AUDIO_I2S_TX, ui16_output);
			g_ui16_tx_idx++;
#endif
			
			if (g_ui16_tx_idx >= AUDIO_FRAME_SIZE)
			{
				g_ui16_tx_idx = 0;
				g_str_bitflag.b1_audio_tx_state = AUDIO_STATE_PLAY;
				
				g_str_audio.ui16_tx_frame_no++;
			}
		}
		
		
	}
}

#endif //ENABLE_AUDIO

