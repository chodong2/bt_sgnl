/*
 *****************************************************************************
 **-------------------------------------------------------------------------**
 **                                                                         **
 **     GSM AMR-NB speech codec   R98   Version 7.6.0   December 12, 2001       **
 **                               R99   Version 3.3.0                       **
 **                               REL-4 Version 4.1.0                       **
 **                                                                         **
 **-------------------------------------------------------------------------**
 *****************************************************************************
 *
 *      File             : vad1.c
 *      Purpose          : Voice Activity Detection (VAD) for AMR (option 1)
 *
 *****************************************************************************
 */

/*
 *****************************************************************************
 *                         MODULE INCLUDE FILE AND VERSION ID
 *****************************************************************************
 */
#include "global_define.h"
#include "vad.h"
const char vad_id[] = "@(#)$Id $" vad_h;

/*
 *****************************************************************************
 *                         INCLUDE FILES
 *****************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>

#include "typedefs.h"
#include "basic_op.h"
#include "enh_formant.h"


Word16 bckr_est[COMPLEN]; /* background noise estimate                */
Word16 ave_level[COMPLEN]; /* averaged input components for stationary */
Word16 old_level[COMPLEN]; /* input levels of the previous frame       */
Word16 sub_level[COMPLEN]; /* input levels calculated at the end of a frame (lookahead)                   */
Word16 a_data5[3][2]; /* memory for the filter bank               */
Word16 a_data3[5]; /* memory for the filter bank               */
Word16 best_corr_hp; /* FIP filtered value Q15                   */
Word16 speech_vad_decision; /* final decision                           */
Word16 complex_warning; /* complex background warning               */
Word16 sp_burst_count; /* counts length of a speech burst incl HO addition                              */
Word16 corr_hp_fast; /* filtered value                           */
Word16 complex_hang_count; /* complex hangover counter, used by VAD    */
Word16 complex_hang_timer; /* hangover initiator, used by CAD          */
Word16 burst_count; /* counts length of a speech burst          */
Word16 hang_count; /* hangover counter                         */
Word16 stat_count; /* stationary counter                       */

/* Note that each of the following three variables (vadreg, pitch and tone)
holds 15 flags. Each flag reserves 1 bit of the variable. The newest
flag is in the bit 15 (assuming that LSB is bit 1 and MSB is bit 16). */
Word16 vadreg; /* flags for intermediate VAD decisions     */
Word16 pitch; /* flags for pitch detection                */
Word16 tone; /* flags for tone detection                 */
Word16 complex_high; /* flags for complex detection              */
Word16 complex_low; /* flags for complex detection              */
Word16 oldlag_count, oldlag; /* variables for pitch detection            */


void init_vad()
{
    int i;

#if 01
    for(i=0;i<COMPLEN;i++)
    {
	    bckr_est[i] = NOISE_INIT;
	    old_level[i] = NOISE_INIT;
	    ave_level[i] = NOISE_INIT;
    }
#else
    memset(bckr_est, (Word16)NOISE_INIT, sizeof(bckr_est));
    memset(old_level, (Word16)NOISE_INIT, sizeof(old_level));
    memset(ave_level, (Word16)NOISE_INIT, sizeof(ave_level));
#endif
    memset(sub_level, 0x00, sizeof(sub_level));
    
    memset(a_data5, 0x00, sizeof(a_data5));
    memset(a_data3, 0x00, sizeof(a_data3));
    
    oldlag_count = 0;
    oldlag = 0;
    pitch = 0;
    tone = 0;

    complex_high = 0;
    complex_low = 0;
    complex_hang_timer = 0;

    vadreg = 0;

    stat_count = 0;
    burst_count = 0;
    hang_count = 0;
    complex_hang_count = 0;

    best_corr_hp = CVAD_LOWPOW_RESET;
    speech_vad_decision = 0;
    complex_warning = 0;
    sp_burst_count = 0;
    corr_hp_fast = CVAD_LOWPOW_RESET;

    return;
}
/*
 *****************************************************************************
 *                         LOCAL VARIABLES AND TABLES
 *****************************************************************************
 */

/*
 ********************************************************************************
 *                         PRIVATE PROGRAM CODE
 ********************************************************************************
 */
/****************************************************************************
 *
 *     Function     : first_filter_stage
 *     Purpose      : Scale input down by one bit. Calculate 5th order
 *                    half-band lowpass/highpass filter pair with
 *                    decimation.
 *
 ***************************************************************************/
static void first_filter_stage(Word16 in[],  /* i   : input signal                  */
Word16 out[],  /* o   : output values, every other    */
/*       output is low-pass part and   */
/*       other is high-pass part every */
Word16 data[] /* i/o : filter memory                 */
)
{
    Word16 temp0, temp1, temp2, temp3, i;
    Word16 data0, data1;

    data0 = data[0];
    data1 = data[1];

    for (i = 0; i < FRAME_LEN / 4; i++)
    {
        temp0 = sub(shr(in[4 *i + 0], 2), mult(COEFF5_1, data0));
        temp1 = add(data0, mult(COEFF5_1, temp0));

        temp3 = sub(shr(in[4 *i + 1], 2), mult(COEFF5_2, data1));
        temp2 = add(data1, mult(COEFF5_2, temp3));

        out[4 *i + 0] = add(temp1, temp2);
        out[4 *i + 1] = sub(temp1, temp2);

        data0 = sub(shr(in[4 *i + 2], 2), mult(COEFF5_1, temp0));
        temp1 = add(temp0, mult(COEFF5_1, data0));

        data1 = sub(shr(in[4 *i + 3], 2), mult(COEFF5_2, temp3));
        temp2 = add(temp3, mult(COEFF5_2, data1));

        out[4 *i + 2] = add(temp1, temp2);
        out[4 *i + 3] = sub(temp1, temp2);
    }

    data[0] = data0;
    data[1] = data1;
}

/****************************************************************************
 *
 *     Function     : filter5
 *     Purpose      : Fifth-order half-band lowpass/highpass filter pair with
 *                    decimation.
 *
 ***************************************************************************/
static void filter5(Word16 *in0,  /* i/o : input values; output low-pass part  */
Word16 *in1,  /* i/o : input values; output high-pass part */
Word16 data[] /* i/o : updated filter memory               */
)
{
    Word16 temp0, temp1, temp2;

    temp0 = sub(*in0, mult(COEFF5_1, data[0]));
    temp1 = add(data[0], mult(COEFF5_1, temp0));
    data[0] = temp0;


    temp0 = sub(*in1, mult(COEFF5_2, data[1]));
    temp2 = add(data[1], mult(COEFF5_2, temp0));
    data[1] = temp0;


    *in0 = shr(add(temp1, temp2), 1);

    *in1 = shr(sub(temp1, temp2), 1);

}

/****************************************************************************
 *
 *     Function     : filter3
 *     Purpose      : Third-order half-band lowpass/highpass filter pair with
 *                    decimation.
 *     Return value : 
 *
 ***************************************************************************/
static void filter3(Word16 *in0,  /* i/o : input values; output low-pass part  */
Word16 *in1,  /* i/o : input values; output high-pass part */
Word16 *data /* i/o : updated filter memory               */
)
{
    Word16 temp1, temp2;

    temp1 = sub(*in1, mult(COEFF3,  *data));
    temp2 = add(*data, mult(COEFF3, temp1));
    *data = temp1;


    *in1 = shr(sub(*in0, temp2), 1);

    *in0 = shr(add(*in0, temp2), 1);

}

/****************************************************************************
 *
 *     Function     : level_calculation
 *     Purpose      : Calculate signal level in a sub-band. Level is calculated
 *                    by summing absolute values of the input data.
 *     Return value : signal level
 *
 ***************************************************************************/
static Word16 level_calculation(Word16 data[],  /* i   : signal buffer                                    */
Word16 *sub_level,  /* i   : level calculate at the end of the previous frame */
/* o   : level of signal calculated from the last         */
/*       (count2 - count1) samples                        */
Word16 count1,  /* i   : number of samples to be counted                  */
Word16 count2,  /* i   : number of samples to be counted                  */
Word16 ind_m,  /* i   : step size for the index of the data buffer       */
Word16 ind_a,  /* i   : starting index of the data buffer                */
Word16 scale /* i   : scaling for the level calculation                */
)
{
    Word32 l_temp1, l_temp2;
    Word16 level, i;

    l_temp1 = 0L;

    for (i = count1; i < count2; i++)
    {
        l_temp1 = L_mac(l_temp1, 1, abs_s(data[ind_m *i + ind_a]));
    }

    l_temp2 = L_add(l_temp1, L_shl(*sub_level, sub(16, scale)));
    *sub_level = extract_h(L_shl(l_temp1, scale));

    for (i = 0; i < count1; i++)
    {
        l_temp2 = L_mac(l_temp2, 1, abs_s(data[ind_m *i + ind_a]));
    }
    level = extract_h(L_shl(l_temp2, scale));

    return level;
}

/****************************************************************************
 *
 *     Function     : filter_bank
 *     Purpose      : Divides input signal into 9-bands and calculas level of
 *                    the signal in each band 
 *
 ***************************************************************************/
static void filter_bank( /* i/o : State struct               */
Word16 in[],  /* i   : input frame                */
Word16 level[] /* 0   : signal levels at each band */
)
{
    Word16 i;
    Word16 tmp_buf[FRAME_LEN];
    int frame_len4 = FRAME_LEN>>2;
    int frame_len8 = FRAME_LEN>>3;
    int frame_len16 = FRAME_LEN>>4;

    /* calculate the filter bank */

    first_filter_stage(in, tmp_buf, a_data5[0]);

    for (i = 0; i < frame_len4; i++)
    {
#if 01
        filter5(&tmp_buf[(i << 2) + 0], &tmp_buf[(i << 2) + 2], a_data5[1]);
        filter5(&tmp_buf[(i << 2) + 1], &tmp_buf[(i << 2) + 3], a_data5[2]);
#else
        filter5(&tmp_buf[4 *i + 0], &tmp_buf[4 *i + 2], a_data5[1]);
        filter5(&tmp_buf[4 *i + 1], &tmp_buf[4 *i + 3], a_data5[2]);
#endif
    }
    for (i = 0; i < frame_len8; i++)
    {
#if 01
        filter3(&tmp_buf[(i << 3) + 0], &tmp_buf[(i << 3) + 4], &a_data3[0]);
        filter3(&tmp_buf[(i << 3) + 2], &tmp_buf[(i << 3) + 6], &a_data3[1]);
        filter3(&tmp_buf[(i << 3) + 3], &tmp_buf[(i << 3) + 7], &a_data3[4]);
#else
        filter3(&tmp_buf[8 *i + 0], &tmp_buf[8 *i + 4], &a_data3[0]);
        filter3(&tmp_buf[8 *i + 2], &tmp_buf[8 *i + 6], &a_data3[1]);
        filter3(&tmp_buf[8 *i + 3], &tmp_buf[8 *i + 7], &a_data3[4]);
#endif
    }

    for (i = 0; i < frame_len16; i++)
    {
#if 01
        filter3(&tmp_buf[16 *i + 0], &tmp_buf[16 *i + 8], &a_data3[2]);
        filter3(&tmp_buf[16 *i + 4], &tmp_buf[16 *i + 12], &a_data3[3]);
#else
        filter3(&tmp_buf[(i << 4) + 0], &tmp_buf[(i << 4) + 8], &a_data3[2]);
        filter3(&tmp_buf[(i << 4) + 4], &tmp_buf[(i << 4) + 12], &a_data3[3]);
#endif
    }

    /* calculate levels in each frequency band */

    /* 3000 - 4000 Hz*/
    level[8] = level_calculation(tmp_buf, &sub_level[8], frame_len4-8, frame_len4, 4, 1, 15);

    /* 2500 - 3000 Hz*/
    level[7] = level_calculation(tmp_buf, &sub_level[7], frame_len8-4, frame_len8, 8, 7, 16);

    /* 2000 - 2500 Hz*/
    level[6] = level_calculation(tmp_buf, &sub_level[6], frame_len8-4, frame_len8, 8, 3, 16);

    /* 1500 - 2000 Hz*/
    level[5] = level_calculation(tmp_buf, &sub_level[5], frame_len8-4, frame_len8, 8, 2, 16);

    /* 1000 - 1500 Hz*/
    level[4] = level_calculation(tmp_buf, &sub_level[4], frame_len8-4, frame_len8, 8, 6, 16);

    /* 750 - 1000 Hz*/
    level[3] = level_calculation(tmp_buf, &sub_level[3], frame_len16-2, frame_len16, 16, 4, 16);

    /* 500 - 750 Hz*/
    level[2] = level_calculation(tmp_buf, &sub_level[2], frame_len16-2, frame_len16, 16, 12, 16);

    /* 250 - 500 Hz*/
    level[1] = level_calculation(tmp_buf, &sub_level[1], frame_len16-2, frame_len16, 16, 8, 16);

    /* 0 - 250 Hz*/
    level[0] = level_calculation(tmp_buf, &sub_level[0], frame_len16-2, frame_len16, 16, 0, 16);

}

/****************************************************************************
 *
 *     Function   : update_cntrl
 *     Purpose    : Control update of the background noise estimate.
 *     Inputs     : pitch:      flags for pitch detection
 *                  stat_count: stationary counter
 *                  tone:       flags indicating presence of a tone
 *                  complex:      flags for complex  detection
 *                  vadreg:     intermediate VAD flags
 *     Output     : stat_count: stationary counter
 *
 ***************************************************************************/
static void update_cntrl( /* i/o : State struct                       */
Word16 level[] /* i   : sub-band levels of the input frame */
)
{
    Word16 i, temp, stat_rat, exp;
    Word16 num, denom;
    Word16 alpha;

    /* handle highband complex signal input  separately       */
    /* if ther has been highband correlation for some time    */
    /* make sure that the VAD update speed is low for a while */

    if (complex_warning != 0)
    {
    
        if (sub(stat_count, CAD_MIN_STAT_COUNT) < 0)
        {
            stat_count = CAD_MIN_STAT_COUNT;
        
        }
    }
    /* NB stat_count is allowed to be decreased by one below again  */
    /* deadlock in speech is not possible unless the signal is very */
    /* complex and need a high rate                                 */

    /* if fullband pitch or tone have been detected for a while, initialize stat_count */




    if ((sub((pitch &0x6000), 0x6000) == 0) || (sub((tone &0x7c00), 0x7c00) == 0))
    {
        stat_count = STAT_COUNT;
    
    }
    else
    {
        /* if 8 last vad-decisions have been "0", reinitialize stat_count */
    
    
        if ((vadreg &0x7f80) == 0)
        {
            stat_count = STAT_COUNT;
        
        }
        else
        {
            stat_rat = 0;
        
            for (i = 0; i < COMPLEN; i++)
            {
            
                if (sub(level[i], ave_level[i]) > 0)
                {
                    num = level[i];
                
                    denom = ave_level[i];
                
                }
                else
                {
                    num = ave_level[i];
                
                    denom = level[i];
                
                }
                /* Limit nimimum value of num and denom to STAT_THR_LEVEL */
            
                if (sub(num, STAT_THR_LEVEL) < 0)
                {
                    num = STAT_THR_LEVEL;
                
                }
            
                if (sub(denom, STAT_THR_LEVEL) < 0)
                {
                    denom = STAT_THR_LEVEL;
                
                }

                exp = norm_s(denom);
                denom = shl(denom, exp);

                /* stat_rat = num/denom * 64 */
                temp = div_s(shr(num, 1), denom);
                stat_rat = add(stat_rat, shr(temp, sub(8, exp)));
            }

            /* compare stat_rat with a threshold and update stat_count */
        
            if (sub(stat_rat, STAT_THR) > 0)
            {
                stat_count = STAT_COUNT;
            
            }
            else
            {
                if ((vadreg &0x4000) != 0)
                {
                    if (stat_count != 0)
                    {
                        stat_count = sub(stat_count, 1);
		    }
                }
            }
        }
    }

    /* Update average amplitude estimate for stationarity estimation */
    alpha = ALPHA4;

    if (sub(stat_count, STAT_COUNT) == 0)
    {
        alpha = 32767;
    }
    else if ((vadreg &0x4000) == 0)
    {
        alpha = ALPHA5;
    }

    for (i = 0; i < COMPLEN; i++)
    {
        ave_level[i] = add(ave_level[i], mult_r(alpha, sub(level[i], ave_level[i])));
    
    }
}

/****************************************************************************
 *
 *     Function     : hangover_addition
 *     Purpose      : Add hangover for complex signal or after speech bursts
 *     Inputs       : burst_count:  counter for the length of speech bursts
 *                    hang_count:   hangover counter
 *                    vadreg:       intermediate VAD decision
 *     Outputs      : burst_count:  counter for the length of speech bursts
 *                    hang_count:   hangover counter
 *     Return value : VAD_flag indicating final VAD decision
 *
 ***************************************************************************/
static Word16 hangover_addition(
/* i/o : State struct                     */
Word16 noise_level,  /* i   : average level of the noise       */
/*       estimates                        */
Word16 low_power /* i   : flag power of the input frame    */
)
{
    Word16 hang_len, burst_len;

    /* 
    Calculate burst_len and hang_len
    burst_len: number of consecutive intermediate vad flags with "1"-decision
    required for hangover addition
    hang_len:  length of the hangover
     */


    if (sub(noise_level, HANG_NOISE_THR) > 0)
    {
        burst_len = BURST_LEN_HIGH_NOISE;
    
        hang_len = HANG_LEN_HIGH_NOISE;
    
    }
    else
    {
        burst_len = BURST_LEN_LOW_NOISE;
    
        hang_len = HANG_LEN_LOW_NOISE;
    
    }

    /* if the input power (pow_sum) is lower than a threshold, clear
    counters and set VAD_flag to "0"  "fast exit"                 */

    if (low_power != 0)
    {
        burst_count = 0;
    
        hang_count = 0;
    
        complex_hang_count = 0;
    
        complex_hang_timer = 0;
    
        return 0;
    }


    if (sub(complex_hang_timer, CVAD_HANG_LIMIT) > 0)
    {
    
        if (sub(complex_hang_count, CVAD_HANG_LENGTH) < 0)
        {
            complex_hang_count = CVAD_HANG_LENGTH;
        
        }
    }

    /* long time very complex signal override VAD output function */

    if (complex_hang_count != 0)
    {
        burst_count = BURST_LEN_HIGH_NOISE;
    
        complex_hang_count = sub(complex_hang_count, 1);
    
        return 1;
    }
    else
    {
        /* let hp_corr work in from a noise_period indicated by the VAD */
    
    
    
        if (((vadreg &0x3ff0) == 0) && (sub(corr_hp_fast, CVAD_THRESH_IN_NOISE) > 0))
        {
            return 1;
        }
    }

    /* update the counters (hang_count, burst_count) */


    if ((vadreg &0x4000) != 0)
    {
        burst_count = add(burst_count, 1);
    
    
        if (sub(burst_count, burst_len) >= 0)
        {
            hang_count = hang_len;
        
        }
        return 1;
    }
    else
    {
        burst_count = 0;
    
    
        if (hang_count > 0)
        {
            hang_count = sub(hang_count, 1);
        
            return 1;
        }
    }
    return 0;
}

/****************************************************************************
 *
 *     Function   : noise_estimate_update
 *     Purpose    : Update of background noise estimate
 *     Inputs     : bckr_est:   background noise estimate
 *                  pitch:      flags for pitch detection
 *                  stat_count: stationary counter
 *     Outputs    : bckr_est:   background noise estimate
 *
 ***************************************************************************/
static void noise_estimate_update(
/* i/o : State struct                       */
Word16 level[] /* i   : sub-band levels of the input frame */
)
{
    Word16 i, alpha_up, alpha_down, bckr_add;

    /* Control update of bckr_est[] */
    update_cntrl(level);

    /* Choose update speed */
    bckr_add = 2;







    if (((0x7800 &vadreg) == 0) && ((pitch &0x7800) == 0) && (complex_hang_count == 0))
    {
        alpha_up = ALPHA_UP1;
    
        alpha_down = ALPHA_DOWN1;
    
    }
    else
    {
    
    
        if ((stat_count == 0) && (complex_hang_count == 0))
        {
            alpha_up = ALPHA_UP2;
        
            alpha_down = ALPHA_DOWN2;
        
        }
        else
        {
            alpha_up = 0;
        
            alpha_down = ALPHA3;
        
            bckr_add = 0;
        
        }
    }

    /* Update noise estimate (bckr_est) */
    for (i = 0; i < COMPLEN; i++)
    {
        Word16 temp;
        temp = sub(old_level[i], bckr_est[i]);

    
        if (temp < 0)
        {
             /* update downwards*/
            bckr_est[i] = add( - 2, add(bckr_est[i], mult_r(alpha_down, temp)));
        

            /* limit minimum value of the noise estimate to NOISE_MIN */
        
            if (sub(bckr_est[i], NOISE_MIN) < 0)
            {
                bckr_est[i] = NOISE_MIN;
            
            }
        }
        else
        {
             /* update upwards */
            bckr_est[i] = add(bckr_add, add(bckr_est[i], mult_r(alpha_up, temp)));
        

            /* limit maximum value of the noise estimate to NOISE_MAX */
        
            if (sub(bckr_est[i], NOISE_MAX) > 0)
            {
                bckr_est[i] = NOISE_MAX;
            
            }
        }
    }

    /* Update signal levels of the previous frame (old_level) */
    for (i = 0; i < COMPLEN; i++)
    {
        old_level[i] = level[i];
    
    }
}

/****************************************************************************
 *
 *     Function   : complex_estimate_adapt
 *     Purpose    : Update/adapt of complex signal estimate
 *     Inputs     : low_power:   low signal power flag 
 *     Outputs    : st->corr_hp_fast:   long term complex signal estimate
 *
 ***************************************************************************/
static void complex_estimate_adapt(
/* i/o : VAD state struct                       */
Word16 low_power /* i   : very low level flag of the input frame */
)
{
    Word16 alpha; /* Q15 */
    Word32 L_tmp; /* Q31 */


    /* adapt speed on own state */

    if (sub(best_corr_hp, corr_hp_fast) < 0)
     /* decrease */
    {
       	// low state  : high state 
        alpha = (sub(corr_hp_fast, CVAD_THRESH_ADAPT_HIGH) < 0)? CVAD_ADAPT_FAST:CVAD_ADAPT_REALLY_FAST;
    }
    else
     /* increase */
    {
        alpha = (sub(corr_hp_fast, CVAD_THRESH_ADAPT_HIGH) < 0)? CVAD_ADAPT_FAST:CVAD_ADAPT_SLOW;
    }

    L_tmp = L_deposit_h(corr_hp_fast);
    L_tmp = L_msu(L_tmp, alpha, corr_hp_fast);
    L_tmp = L_mac(L_tmp, alpha, best_corr_hp);
    corr_hp_fast = round_basic_op(L_tmp); /* Q15 */

    if (sub(corr_hp_fast, CVAD_MIN_CORR) < 0)
    {
        corr_hp_fast = CVAD_MIN_CORR;
    }

    if (low_power != 0)
    {
        corr_hp_fast = CVAD_MIN_CORR;
    }
}

/****************************************************************************
 *
 *     Function     : complex_vad
 *     Purpose      : complex background decision
 *     Return value : the complex background decision
 *
 ***************************************************************************/
static Word16 complex_vad( /* i/o : VAD state struct              */
Word16 low_power /* i   : flag power of the input frame */
)
{
    complex_high = shr(complex_high, 1);
    complex_low = shr(complex_low, 1);

    if (low_power == 0)
    {
        if (sub(corr_hp_fast, CVAD_THRESH_ADAPT_HIGH) > 0)
        {
            complex_high = complex_high | 0x4000;
        }

        if (sub(corr_hp_fast, CVAD_THRESH_ADAPT_LOW) > 0)
        {
            complex_low = complex_low | 0x4000;
        }
    }

    complex_hang_timer = (sub(corr_hp_fast, CVAD_THRESH_HANG) > 0)? add(complex_hang_timer, 1):0;

    return ((sub((complex_high &0x7f80), 0x7f80) == 0) || (sub((complex_low &0x7fff), 0x7fff) == 0));
}

/****************************************************************************
 *
 *     Function     : vad_decision
 *     Purpose      : Calculates VAD_flag
 *     Inputs       : bckr_est:    background noise estimate
 *                    vadreg:      intermediate VAD flags
 *     Outputs      : noise_level: average level of the noise estimates
 *                    vadreg:      intermediate VAD flags
 *     Return value : VAD_flag
 *
 ***************************************************************************/
static Word16 vad_decision(
/* i/o : State struct                       */
Word16 level[COMPLEN],  /* i   : sub-band levels of the input frame */
Word32 pow_sum /* i   : power of the input frame           */
)
{
    Word16 i;
    Word16 snr_sum;
    Word32 L_temp;
    Word16 vad_thr2, temp, noise_level;
    Word16 low_power_flag;

    /* 
    Calculate squared sum of the input levels (level)
    divided by the background noise components (bckr_est).
     */
    L_temp = 0;

    for (i = 0; i < COMPLEN; i++)
    {
        Word16 exp;

        exp = norm_s(bckr_est[i]);
        temp = shl(bckr_est[i], exp);

         //if(temp != 0)
	//if(temp > 0)
        temp = div_s(shr(level[i], 1), temp);

        //temp = shl(temp, sub(exp, UNIRSHFT - 1));
        temp = shl(temp, sub(exp, 5));
        L_temp = L_mac(L_temp, temp, temp);
    }

    snr_sum = extract_h(L_shl(L_temp, 6));
    snr_sum = mult(snr_sum, INV_COMPLEN);

    /* Calculate average level of estimated background noise */
    L_temp = 0;

    for (i = 0; i < COMPLEN; i++)
    {
        L_temp = L_add(L_temp, bckr_est[i]);
    }

    noise_level = extract_h(L_shl(L_temp, 13));

    /* Calculate VAD threshold */
    vad_thr2 = add(mult(VAD_SLOPE, sub(noise_level, VAD_P1)), VAD_THR_HIGH);


    if (sub(vad_thr2, VAD_THR_LOW) < 0)
    {
        vad_thr2 = VAD_THR_LOW;
    
    }

    /* Shift VAD decision register */
    vadreg = shr(vadreg, 1);


    /* Make intermediate VAD decision */

    if (sub(snr_sum, vad_thr2) > 0)
    {
        vadreg = vadreg | 0x4000;
    }
    /* primary vad decsion made */

    /* check if the input power (pow_sum) is lower than a threshold" */

    low_power_flag = (L_sub(pow_sum, VAD_POW_LOW) < 0)? 1:0;

    /* update complex signal estimate st->corr_hp_fast and hangover reset timer using */
    /* low_power_flag and corr_hp_fast  and various adaptation speeds                 */
    complex_estimate_adapt(low_power_flag);

    /* check multiple thresholds of the st->corr_hp_fast value */
    complex_warning = complex_vad(low_power_flag);


    /* Update speech subband vad background noise estimates */
    noise_estimate_update(level);

    /*  Add speech and complex hangover and return speech VAD_flag */
    /*  long term complex hangover may be added */
    speech_vad_decision = hangover_addition(noise_level, low_power_flag);


    return (speech_vad_decision);
}

/****************************************************************************
 *
 *     Function     : vad_complex_detection_update
 *     Purpose      : update vad->bestCorr_hp  complex signal feature state 
 *
 ***************************************************************************/
void vad_complex_detection_update(vadState1 *st,  /* i/o : State struct */
Word16 best_corr_hp /* i   : best Corr    */
)
{
    st->best_corr_hp = best_corr_hp;

}

/****************************************************************************
 *
 *     Function     : vad_tone_detection
 *     Purpose      : Set tone flag if pitch gain is high. This is used to detect
 *                    signaling tones and other signals with high pitch gain.
 *     Inputs       : tone: flags indicating presence of a tone
 *     Outputs      : tone: flags indicating presence of a tone
 *
 ***************************************************************************/
void vad_tone_detection(vadState1 *st,  /* i/o : State struct            */
Word32 t0,  /* i   : autocorrelation maxima  */
Word32 t1 /* i   : energy                  */
)
{
    Word16 temp;
    /* 
    if (t0 > TONE_THR * t1)
    set tone flag
     */
    temp = round_basic_op(t1);



    if ((temp > 0) && (L_msu(t0, temp, TONE_THR) > 0))
    {
        st->tone = st->tone | 0x4000;
    
    
    }
}

/****************************************************************************
 *
 *     Function     : vad_tone_detection_update
 *     Purpose      : Update the tone flag register. Tone flags are shifted right
 *                    by one bit. This function should be called from the speech
 *                    encoder before call to Vad_tone_detection() function.
 *
 ***************************************************************************/
void vad_tone_detection_update(vadState1 *st,  /* i/o : State struct              */
Word16 one_lag_per_frame /* i   : 1 if one open-loop lag is
calculated per each frame,
otherwise 0                     */
)
{
    /* Shift tone flags right by one bit */
    st->tone = shr(st->tone, 1);


    /* If open-loop lag is calculated only once in each frame, do extra update
    and assume that the other tone flag of the frame is one. */
    if (one_lag_per_frame != 0)
    {
        st->tone = shr(st->tone, 1);
        st->tone = st->tone | 0x2000;
    
    
    }
}

/****************************************************************************
 *
 *     Function     : vad_pitch_detection
 *     Purpose      : Test whether signal contains pitch or other periodic
 *                    component.
 *     Return value : Boolean voiced / unvoiced decision in state variable 
 *
 ***************************************************************************/
void vad_pitch_detection(vadState1 *st,  /* i/o : State struct                  */
Word16 T_op[] /* i   : speech encoder open loop lags */
)
{
    Word16 lagcount, i;

    lagcount = 0;


    for (i = 0; i < 2; i++)
    {
    
        if (sub(abs_s(sub(st->oldlag, T_op[i])), LTHRESH) < 0)
        {
            lagcount = add(lagcount, 1);
        }

        /* Save the current LTP lag */
        st->oldlag = T_op[i];
    
    }

    /* Make pitch decision.
    Save flag of the pitch detection to the variable pitch.
     */
    st->pitch = shr(st->pitch, 1);



    if (sub(add(st->oldlag_count, lagcount), NTHRESH) >= 0)
    {
        st->pitch = st->pitch | 0x4000;
    
    
    }

    /* Update oldlagcount */
    st->oldlag_count = lagcount;

}

/****************************************************************************
 *
 *     Function     : vad
 *     Purpose      : Main program for Voice Activity Detection (VAD) for AMR 
 *     Return value : VAD Decision, 1 = speech, 0 = noise
 *
 ***************************************************************************/
Word16 vad1( /* i/o : State struct                 */
Word16 in_buf[] /* i   : samples of the input frame   */
)
{
    Word16 level[COMPLEN];
    Word32 pow_sum;
    Word16 i;

    /* Calculate power of the input frame. */
    pow_sum = 0L;


    for (i = 0; i < FRAME_LEN; i++)
    {
	//pow_sum = L_mac(pow_sum, in_buf[i - LOOKAHEAD], in_buf[i - LOOKAHEAD]);
#if 01
	pow_sum = L_mac(pow_sum, in_buf[i], in_buf[i]);
#else
	pow_sum += ( in_buf[i] * in_buf[i] );
#endif
    }

    /*
    If input power is very low, clear pitch flag of the current frame
     */

    if (L_sub(pow_sum, POW_PITCH_THR) < 0)
    {
        pitch = pitch &0x3fff;
    
    
    }

    /*
    If input power is very low, clear complex flag of the "current" frame
     */

    if (L_sub(pow_sum, POW_COMPLEX_THR) < 0)
    {
        complex_low = complex_low &0x3fff;
    
    
    }

    /*
    Run the filter bank which calculates signal levels at each band
     */
    filter_bank(in_buf, level);

    return (vad_decision(level, pow_sum));
}
