#include "global_define.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// AES
//#include "echo_suppress.h"
#include "stdlib.h"
#include "time.h"
#include "policy.h"


#include "enh_formant.h"
#include "vad.h"
#include "AHDR.h"

int FrameNo;

int fe_init(void)
{
	int i = 0, j = 0, k = 0;
	int options[10];
	
	//short new_ref_speech[FRAMESIZE];
	//short new_pri_speech[FRAMESIZE];
	
	short buf16[FRAMESIZE];
	//short out_speech[FRAMESIZE];
	
	memset(Previn_Buf, 0, sizeof(Previn_Buf));
	memset(Prevsynth_Buf, 0, sizeof(Prevsynth_Buf));
	
	options[0] = 2;
	options[1] = 1;
	options[2] = 1;
	
	Init_fe();
	init_vad();
	reset_AHDR();
	
	for (FrameNo = 0;; FrameNo++)
	{
		memcpy(buf16, (sample+i), sizeof(buf16));//(Word16)new_ref_speech[j];
		i+=FRAMESIZE;

		// Mode : 3 Clusters를 선택하는 옵션 (argv[5]를 통해 0,1,2 중 택)
		FormantEnhancement(buf16, options[0]);
		
		// AHDR
		if (options[1] ==2)
		{
			if (AcousticHighDynamicRange_exe(buf16, options, FRAMESIZE) != NO_ERROR)
			{
				goto EOP;
			}
		}

		if (options[2] == 1)
		{
			// Bio - Acoustic Echo Suppression
			//Echo_suppress(new_ref_speech, new_pri_speech, out_speech);
		}
		else
		{
			//for (j = 0; j < FRAMESIZE; j++)
			//	out_speech[j] = (short)buf16[j];
		}

#if 01
		//memcpy((output+k), buf16, sizeof(buf16));
		for(j=0;j<80;j++)
		{
			ble_uart_send_data( (uint8_t)(buf16[j] & 0x00ff) );
			ble_uart_send_data( (uint8_t)((buf16[j] & 0xff00) >> 8) );
		}
#endif
		k+=FRAMESIZE;
		
		if(k >= 64000)
			break;
	}
		
EOP:
	j = 0;
#if 0
	fclose(ref);
	fclose(pri);
	fclose(out);
	fclose(FE_out);
	
#endif
	return NO_ERROR;
}
