#include "global_define.h"
#include "tm_stm32f4_fft.h"

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>


#include "typedefs.h"
#include "vad.h"
#include "enh_formant.h"
#include "Common.h"
#include "policy.h"
#include "rom_s-peaker.h"
#include "basic_op.h"

#define Hz 62.5F /* 8000/128 */

float32_t Previn_Buf[FFTSIZE];
float32_t Prevsynth_Buf[FFTSIZE];
#if 01
float32_t FFT_Buf[FFTSIZE];
#else
float32_t FFT_Buf[FFTSIZE*2];
float32_t oFFT_Buf[FFTSIZE];
#endif
float32_t preemph_last_FE;
float32_t deemph_last_FE;

float32_t *pOverlap;

Word16 old_A[11];


void Init_fe(void)
{
	pOverlap = FFT_Buf + FRONTMARGIN;
	
	memset(old_A, 0x00, sizeof(old_A));
	// For LPC coefficient
	old_A[0] = 4096;
	
	preemph_last_FE = 0.f;
	deemph_last_FE = 0.f;
	
	memset(Previn_Buf, 0, sizeof(Previn_Buf));
	memset(Prevsynth_Buf, 0, sizeof(Prevsynth_Buf));
}

void FormantEnhancement(Word16 *Signal, int Mode)
{
	Word16 Signal_AC[FRONTMARGIN + FRAMESIZE];
	Word16 SignalVAD[FRAMESIZE] = {0,};
	//Word16 rc[4];
	Word16 LPC[44], rHigh[11], rLow[11];
	
	
	int Final, Apt;
	int idx, idx_1, idx_2, cnt = 0, cnt2 = 0, cnt3 = 0,idx_3, idx_4, flag = 0;
	int Formant[M];
	//int countreal = 0;
	//int countimg = 0;
	int Valley_buf[10];
	int Size;
	int MinimumGap_idx;
	int MinimumGap;
	int HarValley[WINDOW_BY_TWO], HarSize;
	int Gap[M];
	int ApplyRange[M][2];
	int Detect;
	
	float32_t Out_Buf[FRAMESIZE];
	float32_t FFTabs[WINDOW_BY_TWO];
#if 01
	float32_t FFT_LPC[FFTSIZE];
	float32_t FFT_LPC2[FFTSIZE];
#else
	float32_t FFT_LPC[FFTSIZE*2];
#endif
	float32_t FE_SignalReal[WINDOW_BY_TWO];
	float32_t FE_SignalIMG[WINDOW_BY_TWO];
	float32_t Norm[WINDOW_BY_TWO];
	float32_t Roots_Positive[10][2];
	float32_t angz[10];
	float32_t Eqn[EQNSIZE + 1];
	float32_t Roots[EQNSIZE] = {0,};
	float32_t Roots_I[EQNSIZE][2] = {0,};
	float32_t BandWidth[M];
	float32_t Formants[M];
	float32_t FormantsNew[M], BandWidthNew[M];
	float32_t FF_BW[M][2];
	
	//float GainSize_One = 0;
	float32_t Valley = 0;
	float32_t GainSize = 0;
	float32_t DC, FO;
	//float Peaks[10][2];
	
	float32_t *pSynth_Buf;
	float32_t *pGainFilter;
	float32_t sqrt_out;
	
	TM_FFT_F32_t FFT;    /*!< FFT structure */
	pGainFilter = FFT_LPC;

	for (idx_1 = 0; idx_1 < M; idx_1++)
	{
		for (idx_2 = 0; idx_2 < 2; idx_2++)
			Roots_Positive[idx_1][idx_2] = 0;
		
		angz[idx_1] = 0;
		BandWidth[idx_1] =  - 10;
		Formants[idx_1] =  - 10;
		Formant[idx_1] =  - 10;
		
		FF_BW[idx_1][0] =  - 10;
		FF_BW[idx_1][1] =  - 10;
		
		Valley_buf[idx_1] =  - 10;
		
		ApplyRange[idx_1][0] =  - 10;
		ApplyRange[idx_1][1] =  - 10;
	}
	
	// LPC 버퍼 초기화
	memset(LPC, 0x00, sizeof(LPC));
	memset(Eqn, 0x00, sizeof(Eqn));
	
	// LPC의 FFT를 위한 버퍼 초기화
	memset(LPC, 0x00, sizeof(LPC));
	memset(FFT_LPC, 0x00, sizeof(FFT_LPC));
	memset(FFT_LPC2, 0x00, sizeof(FFT_LPC2));
	
	memset(FFT_Buf, 0x00, sizeof(FFT_Buf));
	// Formant Enhancement를 하기전에 Overlap-add를 위해 Frame 설정
	for (idx = 0; idx < FRAMESIZE; idx++)
		pOverlap[idx] = (float32_t)Signal[idx];
	
	preemphasis(FRAMESIZE, pOverlap, &preemph_last_FE);
	
	BufferCopy(Previn_Buf + FRAMESIZE, FFT_Buf, FRONTMARGIN);
	
	// Autocorrelation 및 VAD를 위한 신호를 각각의 버퍼에 저장
	for (idx = 0; idx < FRONTMARGIN + FRAMESIZE; idx++)
	{
		Signal_AC[idx] = (Word16)FFT_Buf[idx];
		if (idx >= FRONTMARGIN && idx < FRONTMARGIN + FRAMESIZE)
			SignalVAD[idx - FRONTMARGIN] = (Word16)(FFT_Buf[idx]);
	}
	
	// 다음 Overlap-add를 위해 FFT_Buf의 내용을 Previn_Buf에 저장
	BufferCopy(FFT_Buf, Previn_Buf, FFTSIZE);
	
	// Window를 적용
	Apply_Window(FFT_Buf, Window, FFTSIZE);
	
	// VAD 루틴
	// 음성이 존재하는 구간만 Formant Enhancement
	if ( vad1(SignalVAD) )
	{
		// LPC를 구하기 위한 루틴
		Autocorr(Signal_AC, M, rHigh, rLow, Window);
		Levinson(old_A, rHigh, rLow, LPC);
		
#if 0
		for (idx = 0; idx < FFTSIZE; idx++)
		{
			if (idx < 11)
			{
				FFT_LPC[idx] = (float)LPC[idx] / 4096.f;
			}
			else
			{
				FFT_LPC[idx] = 0;
			}
		}
#else
		memset(FFT_LPC, 0x00, sizeof(FFT_LPC));
		for (idx = 0; idx < 11; idx++)
		{
			Eqn[idx] = FFT_LPC[idx] = (float)LPC[idx] / 4096.f;
		}
#endif
		
		//memcpy(Eqn, FFT_LPC, sizeof(Eqn));
		
		// LPC Coefficient로 구성된 다항식의 실근 혹은 복소수근을 구하는 Bairstow Method 루틴
		// 여기서 pow(0.1, 10)가 정확도를 의미.
		bairstow(Eqn, Roots, Roots_I, (float32_t)pow(0.1, 2));
		
		for (idx = 0; idx < M; idx++)
		{
			if (fabs(Roots_I[idx][0]) > 1.0 || fabs(Roots_I[idx][1]) > 1.0)
				bairstow(Eqn, Roots, Roots_I, (float32_t)pow(0.1, 2));
		}
		
		// 실근과 복소수근을 하나로 통일하기 위한 루틴
		for (idx_1 = 0; idx_1 < M; idx_1++)
		{
			if (Roots[idx_1] != 0)
			{
				for (idx_2 = 0; idx_2 < M; idx_2++)
				{
					if (Roots_I[idx_2][0] == 0 && Roots_I[idx_2][1] == 0)
					{
						Roots_I[idx_2][0] = Roots[idx_1];
						break;
					}
				}
			}
		}
		
		// 위에서 구한 근을 오름차순으로 정렬하는 루틴
		SortArray(Roots_I);
		
		// 복소수근의 허수가 양수인 값만 이용하므로, 해당 값만 저장하는 루틴
		for (idx = 0; idx < M; idx++)
		{
			if (Roots_I[idx][1] >= 0 || (Roots_I[idx][0] != 0 && Roots_I[idx][1] == 0))
			{
				Roots_Positive[cnt2][0] = Roots_I[idx][0];
				Roots_Positive[cnt2][1] = Roots_I[idx][1];
				cnt2++;
			}
		}
		
		if (cnt2 == 11)
			Roots_Positive[cnt2][0] = 100;
		
		// Formant Location을 위한 루틴으로 Formant 및 Formant의 Bandwidth를 구하는 루틴
		for (idx = 0; idx < M; idx++)
		{
			if (Roots_Positive[idx][0] == 0 && Roots_Positive[idx][1] == 0)
				break;
			
			angz[idx] = (float32_t)atan2(Roots_Positive[idx][1], Roots_Positive[idx][0]);
#if 01
			arm_sqrt_f32(Roots_Positive[idx][1] *Roots_Positive[idx][1] + Roots_Positive[idx][0] *Roots_Positive[idx][0],&sqrt_out);
			BandWidth[idx] = (float32_t)( - 1 *(Fs / PI1) *log(sqrt_out));
#else
			BandWidth[idx] = (float32_t)( - 1 *(Fs / PI1) *log(sqrt(Roots_Positive[idx][1] *Roots_Positive[idx][1] + Roots_Positive[idx][0] *Roots_Positive[idx][0])));
#endif
			BandWidth[idx] = (float32_t)floor(BandWidth[idx] / Hz + (float32_t)0.5);
			
			if ((int)BandWidth[idx] % 2 == 0)
			{
				BandWidth[idx] -= 1;
			}
			
			Formants[idx] = (float32_t)floor(((angz[idx]*(Fs / PI2)) / Hz) + (float32_t)0.5);
		}
		
		// Formant Location을 통해 구한 Formant 및 Formant Bandwidth를 오름차순으로 정렬하는 루틴
		SortArraySingle(Formants, BandWidth, M);
		cnt = 0;
		for (idx = 0; idx < M; idx++)
		{
			if (Formants[idx] !=  - 10)
				cnt++;
		}
		
		for (idx = 0; idx < cnt; idx++)
		{
			FormantsNew[cnt - idx - 1] = Formants[M - idx - 1];
			BandWidthNew[cnt - idx - 1] = BandWidth[M - idx - 1];
		}
		Size = cnt;
		
#ifdef FPU_CAL
		// LPC Spectrum을 구하기 위해 LPC를 FFT하는 루틴
		rn_fft(FFT_LPC, 1);
		FE_SignalReal[0] = FFT_LPC[0];
		FE_SignalIMG[0] = 0;
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FE_SignalReal[idx] = FFT_LPC[(idx << 1)];
			FE_SignalIMG[idx]  = FFT_LPC[(idx << 1) + 1];
		}
		FE_SignalReal[WINDOW_BY_TWO - 1] = FFT_LPC[1];
		FE_SignalIMG[WINDOW_BY_TWO - 1] = 0;		
#else
		for (idx = 0; idx < FFTSIZE; idx++)
		{
			FFT_LPC[idx] = (float32_t)((float32_t)FFT_LPC[idx] * 8388607.f);
		}
		r_fft(FFT_LPC, 1);
		
		
		FE_SignalReal[0] = FFT_LPC[0] / 8388607.f;
		FE_SignalIMG[0] = 0;
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FE_SignalReal[idx] = FFT_LPC[(idx << 1)] / 8388607.f;
			FE_SignalIMG[idx]  = FFT_LPC[(idx << 1) + 1] / 8388607.f;
		}
		FE_SignalReal[WINDOW_BY_TWO - 1] = FFT_LPC[1] / 8388607.f;
		FE_SignalIMG[WINDOW_BY_TWO - 1] = 0;
#endif
#if 0
		//arm_cfft_f32(&arm_cfft_sR_f32_len128, FFT_LPC, 0 , 1);
		//arm_cmplx_mag_f32(FFT_LPC, oFFT_Buf, FFTSIZE);
		//memcpy(FFT_LPC, oFFT_Buf, sizeof(oFFT_Buf));

		
		/* Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation */
		TM_FFT_Init_F32(&FFT, FFTSIZE, 0);
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		TM_FFT_SetBuffers_F32(&FFT, FFT_LPC, oFFT_Buf);
		
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);

		memcpy(FFT_LPC, oFFT_Buf, sizeof(oFFT_Buf));
#endif
	
		
		// LPC Spectrum을 구하기 위한 루틴
		for (idx = 0; idx < WINDOW_BY_TWO; idx++)
		{
#if 01
			arm_sqrt_f32(FE_SignalReal[idx] * FE_SignalReal[idx] + FE_SignalIMG[idx] * FE_SignalIMG[idx],&sqrt_out);
			Norm[idx] = (float32_t)log(1 / sqrt_out);
#else
			Norm[idx] = (float32_t)log(1 / sqrt(FE_SignalReal[idx] * FE_SignalReal[idx] + FE_SignalIMG[idx] * FE_SignalIMG[idx]));
#endif
		}
		
#if 01
		// 음성신호를 FFT하는 루틴
		r_fft(FFT_Buf, 1);
#else
		
		/* Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation */
		TM_FFT_Init_F32(&FFT, FFTSIZE, 0);
		
		/* We didn't used malloc for allocation, so we have to set pointers ourself */
		/* Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part */
		/* Output buffer must be FFT_SIZE in length */
		TM_FFT_SetBuffers_F32(&FFT, FFT_Buf, oFFT_Buf);
		
		/* Do FFT on signal, values at each bin and calculate max value and index where max value happened */
		TM_FFT_Process_F32(&FFT);

		memcpy(FFT_Buf, oFFT_Buf, sizeof(oFFT_Buf));
#endif
		
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FE_SignalReal[idx] = FFT_Buf[(idx << 1)];
			FE_SignalIMG[idx]  = FFT_Buf[(idx << 1) + 1];
		}
		
		// Search Harmonic Valley
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
#if 01
			arm_sqrt_f32(FE_SignalReal[idx] *FE_SignalReal[idx] + FE_SignalIMG[idx] *FE_SignalIMG[idx],&sqrt_out);
			FFTabs[idx] = (float32_t)log(sqrt_out);
#else
			FFTabs[idx] = (float32_t)log(sqrt(FE_SignalReal[idx] *FE_SignalReal[idx] + FE_SignalIMG[idx] *FE_SignalIMG[idx]));
#endif
		}


		// 구해진 LPC Spectrum을 통해 Formant Frequency를 구하는 루틴
		cnt = 0;
		cnt2 = 0;
		cnt3 = 0;
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			if (Norm[0] > Norm[1] && idx == 1)
				Formant[cnt++] = 0;
			
			if ((Norm[idx - 1] < Norm[idx]) && (Norm[idx] > Norm[idx + 1]))
				Formant[cnt++] = idx;
			
			if ((Norm[idx - 1] > Norm[idx]) && (Norm[idx] < Norm[idx + 1]))
				Valley_buf[cnt2++] = idx;

			if ((idx > 1) && (idx < WINDOW_BY_TWO - 2) && (FFTabs[idx] < FFTabs[idx - 1]) && (FFTabs[idx] < FFTabs[idx + 1]))//find harmonic valley
				HarValley[cnt3++] = idx;
		}
		HarSize = cnt3;

		// Formant Frequency 및 Bandwidth를 결정하는 루틴
		for (idx = 0; idx < M; idx++)
		{
			if (Formant[idx] ==  - 10)
				break;
			for (idx_1 = 0; idx_1 < Size; idx_1++)
			{
				Gap[idx_1] = abs(Formant[idx] - (int)FormantsNew[idx_1]);
			}
			MinimumGap = Gap[0];
			MinimumGap_idx = 0;
			
			for (idx_2 = 1; idx_2 < Size; idx_2++)
			{
				if (MinimumGap > Gap[idx_2])
				{
					MinimumGap = Gap[idx_2];
					MinimumGap_idx = idx_2;
				}
			}
			
			if (idx == 0 && FormantsNew[0] == 0 && Valley_buf[0] - Formant[0] < (int)(floor(BandWidthNew[MinimumGap_idx] / (float32_t)2.0)))
			{
				for (idx_3 = 0; idx_3 < Size - 1; idx_3++)
				{
					FormantsNew[idx_3] = FormantsNew[idx_3 + 1];
					BandWidthNew[idx_3] = BandWidthNew[idx_3 + 1];
				}
				FormantsNew[idx_3] =  - 10;
				BandWidthNew[idx_3] =  - 10;
				
				idx--;
				continue;
			}
			
			for (idx_3 = 0; idx_3 < cnt2; idx_3++)
			{
				if (Valley_buf[idx_3] < FormantsNew[MinimumGap_idx] && Valley_buf[idx_3 + 1] > FormantsNew[MinimumGap_idx])
				{
					if ((Valley_buf[idx_3 + 1] - Valley_buf[idx_3] + 1) < BandWidthNew[MinimumGap_idx] && BandWidthNew[MinimumGap_idx] > FFTSIZE_BY_TWO)
					{
						
						for (idx_4 = MinimumGap_idx; idx_4 < Size - 1; idx_4++)
						{
							FormantsNew[idx_4] = FormantsNew[idx_4 + 1];
							BandWidthNew[idx_4] = BandWidthNew[idx_4 + 1];
						}
						FormantsNew[idx_4] =  - 10;
						BandWidthNew[idx_4] =  - 10;
						
						flag = 1;
					}
				}
			}
			if (flag == 1)
			{
				flag = 0;
				idx--;
				continue;
			}
			
			FF_BW[idx][0] = (float32_t)Formant[idx];
			FF_BW[idx][1] = BandWidthNew[MinimumGap_idx];
			
		}
		Final = idx;
		
		if ((int)FF_BW[idx - 1][0] > Valley_buf[cnt2 - 1] && ((int)FF_BW[idx - 1][0] - Valley_buf[cnt2 - 1]) < (int)floor(FF_BW[idx - 1][1] / (float32_t)2.0))
		{
			FF_BW[idx - 1][0] =  - 10;
			FF_BW[idx - 1][1] =  - 10;
		}
		
		// 첫번째 Formant의 오류를 검사하고 오류라면 없애는 루틴
		if (FF_BW[0][0] == 0)
		{
			for (idx = 0; idx < FFTSIZE_BY_TWO-1; idx++)
			{
				if ((Norm[idx] - Norm[idx + 1]) < 0)
				{
					Valley = (float32_t)idx;
					break;
				}
			}
			
			if (Valley - FF_BW[0][0] < floor(FF_BW[0][1] / 2))
			{
				FF_BW[0][0] =  - 10.0;
				FF_BW[0][1] =  - 10.0;
			}
		}
		
		for (idx = 0; idx < M; idx++)
		{
			if (FF_BW[idx][1] == 1)
			{
				FF_BW[idx][1] = 3.0;
			}
		}
		
		// 결정된 Formant Frequency 및 Formant Bandwidth를 개선시키는 루틴.
		// 추정된 Formant Bandwidth 인근에 있는 Harmonics Valley를 찾아서 Bandwidth를 더욱 정확하게 추정한다.
		for (idx = 0; idx < M; idx++)
		{
			if (((int)FF_BW[idx][0] < 0 || (int)FF_BW[idx][1] < 0) || (isnan(FF_BW[idx][1]) == 1) )// || _fpclass(FF_BW[idx][1]) == _FPCLASS_PINF)
				//if (((int)FF_BW[idx][0] < 0 || (int)FF_BW[idx][1] < 0) || (isnan(FF_BW[idx][1]) == 1) || _fpclass(FF_BW[idx][1]) == 0x0200)
				continue;
			
			Detect = (int)FF_BW[idx][0];
			
			switch (Detect)
			{
			case 0:
				if (FF_BW[idx][0] == (float32_t)0.0)
				{
					for (idx_1 = 0; idx_1 < (int)FF_BW[idx + 1][0]; idx_1++)
						if (Norm[idx_1] < Norm[idx_1 + 1])
							break;
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						if ((int)floor(FF_BW[idx][1] / 2) < HarValley[idx_2] && HarValley[idx_2] < idx_1)
							break;
					
					FF_BW[idx][1] = (float32_t)(2 *HarValley[idx_2] + 1);
					ApplyRange[idx][0] = 0;
					ApplyRange[idx][1] = HarValley[idx_2];
				}
				else
				{
					for (idx_1 = HarSize; idx_1 != 0; idx_1--)
					{
						if ((int)Valley < HarValley[idx_1 - 1] && HarValley[idx_1 - 1] < (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] / 2)))
						{
							ApplyRange[idx][0] = HarValley[idx_1 - 1];
							break;
						}
					}
					
					for (idx_1 = 0; idx_1 < cnt2; idx_1++)
						if ((int)FF_BW[idx][0] < Valley_buf[idx_1] && Valley_buf[idx_1] < (int)FF_BW[idx + 1][0])
							break;
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
					{
						if ((int)(FF_BW[idx][0] + floor(FF_BW[idx][1] / 2)) < HarValley[idx_2] && HarValley[idx_2] < Valley_buf[idx_1])
						{
							ApplyRange[idx][1] = HarValley[idx_2];
							break;
						}
					}
					
					if (ApplyRange[idx][0] ==  - 10)
					{
						ApplyRange[idx][0] = (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] / 2+(float32_t)0.5));
						
						for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						{
							if (ApplyRange[idx][0] < HarValley[idx_2] && HarValley[idx_2] < FF_BW[idx][0])
							{
								ApplyRange[idx][0] = HarValley[idx_2];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][1] ==  - 10)
					{
						ApplyRange[idx][1] = (int)(FF_BW[idx][0] + floor(FF_BW[idx][1] / 2+(float32_t)0.5));
						
						for (idx_2 = HarSize; idx_2 != 0; idx_2--)
						{
							if (FF_BW[idx][0] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < ApplyRange[idx][1])
							{
								ApplyRange[idx][1] = HarValley[idx_2 - 1];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][0] < 0)
						ApplyRange[idx][0] = 0;
					
					FF_BW[idx][1] = 2 *((float32_t)ApplyRange[idx][1] - FF_BW[idx][0]) + 1;
					
				}
				
				continue;
			default:
				if ((idx != Final - 1) || (idx == Final - 1 && Valley_buf[cnt2 - 1] > (int)FF_BW[idx][0]))
				{
					for (idx_1 = 0; idx_1 < cnt2; idx_1++)
						if (FF_BW[idx - 1][0] < Valley_buf[idx_1] && Valley_buf[idx_1] < FF_BW[idx][0])
							break;
					
					for (idx_2 = HarSize; idx_2 != 0; idx_2--)
					{
						if (Valley_buf[idx_1] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] / 2)))
						{
							ApplyRange[idx][0] = HarValley[idx_2 - 1];
							break;
						}
					}
					
					for (idx_1 = 0; idx_1 < cnt2; idx_1++)
						if ((int)FF_BW[idx][0] < Valley_buf[idx_1] && Valley_buf[idx_1] < (int)FF_BW[idx + 1][0])
							break;
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
					{
						if ((int)(FF_BW[idx][0] + floor(FF_BW[idx][1] / 2)) < HarValley[idx_2] && HarValley[idx_2] < Valley_buf[idx_1])
						{
							ApplyRange[idx][1] = HarValley[idx_2];
							break;
						}
					}
					
					if (ApplyRange[idx][0] ==  - 10)
					{
						ApplyRange[idx][0] = (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] / 2+(float32_t)0.5));
						
						for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						{
							if (ApplyRange[idx][0] < HarValley[idx_2] && HarValley[idx_2] < FF_BW[idx][0])
							{
								ApplyRange[idx][0] = HarValley[idx_2];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][1] ==  - 10)
					{
						ApplyRange[idx][1] = (int)(FF_BW[idx][0] + floor(FF_BW[idx][1] / 2+(float32_t)0.5));
						
						for (idx_2 = HarSize; idx_2 != 0; idx_2--)
						{
							if (FF_BW[idx][0] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < ApplyRange[idx][1])
							{
								ApplyRange[idx][1] = HarValley[idx_2 - 1];
								break;
							}
						}
					}
					
					if ((int)FF_BW[idx][0] - ApplyRange[idx][0] < ApplyRange[idx][1] - (int)FF_BW[idx][0])
						FF_BW[idx][1] = 2 *((float32_t)ApplyRange[idx][1] - FF_BW[idx][0]) + 1;
					else
					{
						FF_BW[idx][1] = 2 *(FF_BW[idx][0] - (float32_t)ApplyRange[idx][0]) + 1;
					}
				}
				else if (idx == Final - 1 && Valley_buf[cnt2 - 1] < (int)FF_BW[idx][0])
				{
					for (idx_2 = HarSize; idx_2 != 0; idx_2--)
					{
						if (Valley_buf[cnt2 - 1] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] / 2)))
						{
							ApplyRange[idx][0] = HarValley[idx_2 - 1];
							break;
						}
					}
					
					for (idx_2 = 0; idx_2 < HarSize; idx_2++)
					{
						if ((int)(FF_BW[idx][0] + floor(FF_BW[idx][1] / 2)) < HarValley[idx_2] && HarValley[idx_2] < FFTSIZE_BY_TWO)
						{
							ApplyRange[idx][1] = HarValley[idx_2];
							break;
						}
					}
					
					if (ApplyRange[idx][0] ==  - 10)
					{
						ApplyRange[idx][0] = (int)(FF_BW[idx][0] - floor(FF_BW[idx][1] / 2+(float32_t)0.5));
						
						for (idx_2 = 0; idx_2 < HarSize; idx_2++)
						{
							if (ApplyRange[idx][0] < HarValley[idx_2] && HarValley[idx_2] < FF_BW[idx][0])
							{
								ApplyRange[idx][0] = HarValley[idx_2];
								break;
							}
						}
					}
					
					if (ApplyRange[idx][1] ==  - 10)
					{
						ApplyRange[idx][1] = (int)(FF_BW[idx][0] + floor(FF_BW[idx][1] / 2+(float32_t)0.5));
						
						for (idx_2 = HarSize; idx_2 != 0; idx_2--)
						{
							if (FF_BW[idx][0] < HarValley[idx_2 - 1] && HarValley[idx_2 - 1] < ApplyRange[idx][1])
							{
								ApplyRange[idx][1] = HarValley[idx_2 - 1];
								break;
							}
						}
					}
					
					if ((int)FF_BW[idx][0] - ApplyRange[idx][0] < ApplyRange[idx][1] - (int)FF_BW[idx][0])
						FF_BW[idx][1] = 2 *((float32_t)ApplyRange[idx][1] - FF_BW[idx][0]) + 1;
					else
					{
						FF_BW[idx][1] = 2 *(FF_BW[idx][0] - (float32_t)ApplyRange[idx][0]) + 1;
						
					}
				}
			}
		}
		
		// Formant Enhancement를 적용하는 루틴
		DC = FE_SignalReal[0];
		FO = FE_SignalIMG[0];
		
		for (idx_1 = 0; idx_1 < FORMANTNUM; idx_1++)
		{
			if (((int)FF_BW[idx_1][0] < 0 || (int)FF_BW[idx_1][1] < 0) || (isnan(FF_BW[idx_1][1]) == 1) )//|| _fpclass(FF_BW[idx_1][1]) == _FPCLASS_PINF)
				//if (((int)FF_BW[idx_1][0] < 0 || (int)FF_BW[idx_1][1] < 0) || (isnan(FF_BW[idx_1][1]) == 1) || fpclass(FF_BW[idx_1][1]) == 0x0200)
				continue;
			
			if (SKS_FR[(int)FF_BW[idx_1][0]][Mode] > 3)
				GainSize = 3;
			else
				GainSize = SKS_FR[(int)FF_BW[idx_1][0]][Mode];
			
			Apt = ApplyRange[idx_1][1] - ApplyRange[idx_1][0] + 1;
			
			// Gain Window를 생성하는 루틴
			HammingGainFilter((float32_t)Apt, pGainFilter, GainSize);
			
			if ( ( (ApplyRange[idx_1][0] > 63) || (ApplyRange[idx_1][1] > 63) ) //63 -> (FFTSIZE_BY_TWO - 1)
			    || ( (ApplyRange[idx_1][0] < 0) || (ApplyRange[idx_1][1] < 0) ) )
				continue;
			
			cnt = 0;
			for (idx = ApplyRange[idx_1][0]; idx < ApplyRange[idx_1][1] + 1; idx++)
			{
				FE_SignalReal[idx] *= pGainFilter[cnt];
				FE_SignalIMG[idx] *= pGainFilter[cnt];
				cnt++;
			}
		}
		
		FE_SignalReal[0] = DC;
		FE_SignalIMG[0] = FO;
		
		for (idx = 1; idx < WINDOW_BY_TWO - 1; idx++)
		{
			FFT_Buf[2 * idx] = FE_SignalReal[idx];
			FFT_Buf[2 * idx + 1] = FE_SignalIMG[idx];
		}
		
		r_fft(FFT_Buf, -1);
	}

	pSynth_Buf = FFT_Buf;
	
	int overlap = FFTSIZE - FRAMESIZE;
	// Overlap-add를 위한 루틴
	for (idx = 0; idx < overlap; idx++)
	{
		Out_Buf[idx] = pSynth_Buf[idx] + Prevsynth_Buf[idx + FRAMESIZE];
	}

	memcpy(Out_Buf+overlap, pSynth_Buf+overlap, (FRAMESIZE-overlap)*sizeof(float32_t));
	
	BufferCopy(pSynth_Buf, Prevsynth_Buf, FFTSIZE);
	
	deemphasis(FRAMESIZE, Out_Buf, &deemph_last_FE);
	
	// Overflow가 되면 최대치로 고정시켜주는 루틴
	for (idx = 0; idx < FRAMESIZE; idx++)
	{
		if (Out_Buf[idx] > MAX_16)
			Signal[idx] = MAX_16;
		else if (Out_Buf[idx] <  MIN_16)
			Signal[idx] = MIN_16;
		else
			Signal[idx] = (Word16)(Out_Buf[idx] + 0.5f);
	}
	
	return;
}

void Apply_Window(float32_t *buf, float32_t *w, short size)
{
	short i;
	
	for (i = 0; i < size; i++)
		buf[i] *= w[i];
}

void HammingGainFilter(float32_t BW, float32_t *GainFilter, float32_t GainSize)
{
	short idx1;
	float32_t val0, val1;
	
#if 0
	val1 = (float32_t)(0.54 - 0.46 * cos(PI2 *(1 / (BW + 1))));
#else
	val1 = (float32_t)(0.54f - 0.46f * arm_cos_f32(PI2 *(1 / (BW + 1))));
#endif
	for (idx1 = 0; idx1 < BW; idx1++)
	{
#if 0
		val0 = (float32_t)(0.54 - 0.46 * cos(PI2 *((idx1 + 1) / (BW + 1))));
#else
		val0 = (float32_t)(0.54f - 0.46f * arm_cos_f32(PI2 *((idx1 + 1) / (BW + 1))));
#endif
		GainFilter[idx1] = (float32_t)pow((val0 - val1) / (1 - val1) + 1, GainSize);
	}
	
	return;
}

void bairstow(float32_t *equ, float32_t *arroot_R, float32_t arroot_I[][2], float32_t Accuracy)
{
	float32_t r, s;
	float32_t r_last, s_last;
	float32_t dr, ds;
	float32_t b[EQNSIZE + 1];
	float32_t c[EQNSIZE + 1];
	float32_t buffer_b[EQNSIZE + 1];
	int i, j, k;
	int n, m;
	float32_t sqrt_out;
	
	memcpy(buffer_b, equ, (EQNSIZE + 1) *sizeof(float32_t));
	
	for (i = 0; i <= EQNSIZE; i++)
	{
		if (buffer_b[i] == 0)
		{
#if 01
			buffer_b[i] += 0.00000001f;
#else
			buffer_b[i] = (float32_t)pow(10.0, -8.0);
#endif
		}
	}
	
	n = EQNSIZE;
	m = EQNSIZE;
	
	float32_t pre_cal = 0.0;
	for (j = 0; j < (n / 2); j++)
	{
		float32_t val0;
		
		r_last = (-1)*(buffer_b[1]) / (buffer_b[0]);
		s_last = (-1)*(buffer_b[2]) / (buffer_b[0]);
		
		if (m == 2 && j == 0)
		{
			r = r_last;
			s = s_last;
			
			pre_cal = (float32_t)(r *r + 4 * s);
			val0 = r * 0.5f;
			if (pre_cal > 0)
			{
#if 01
				arm_sqrt_f32(pre_cal,&sqrt_out);
				arroot_R[(j << 1)] = val0 + (sqrt_out * 0.5f);
				arroot_R[(j << 1) + 1] = val0 - (sqrt_out * 0.5f);
#else
				arroot_R[2 * j] = val0 + ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
				arroot_R[2 * j + 1] = val0 - ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
#endif
			}
			else if (pre_cal == 0)
			{
				arroot_R[(j << 1)] = val0;
				arroot_R[(j << 1) + 1] = val0;
			}
			else if (pre_cal < 0)
			{
#if 01
				arm_sqrt_f32((-1)*pre_cal,&sqrt_out);
				arroot_I[(j << 1)][0] = val0;
				arroot_I[(j << 1)][1] = sqrt_out * (-0.5f);
				arroot_I[(j << 1) + 1][0] = val0;
				arroot_I[(j << 1) + 1][1] = sqrt_out * 0.5f;
#else
				arroot_I[2 * j][0] = val0;
				arroot_I[2 * j][1] = (-1)*((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
				arroot_I[2 * j + 1][0] = val0;
				arroot_I[2 * j + 1][1] = ((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
#endif
			}
		}
		else if (m == 2 && j != 0)
		{
			r = (-1)*(b[1]) / (b[0]);
			s = (-1)*(b[2]) / (b[0]);
			
			val0 = r * 0.5f;
			pre_cal = (float32_t)(r *r + 4 * s);
			if (pre_cal > 0)
			{
#if 01
				arm_sqrt_f32(pre_cal,&sqrt_out);
				arroot_R[(j << 1)] = val0 + (sqrt_out * 0.5f);
				arroot_R[(j << 1) + 1] = val0 - (sqrt_out * 0.5f);
#else
				arroot_R[2 * j] = val0 + ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
				arroot_R[2 * j + 1] = val0 - ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
#endif
			}
			else if (pre_cal == 0)
			{
				arroot_R[(j << 1)] = val0;
				arroot_R[(j << 1) + 1] = val0;
			}
			else if (pre_cal < 0)
			{
#if 01
				arm_sqrt_f32((-1)*pre_cal,&sqrt_out);
				arroot_I[(j << 1)][0] = val0;
				arroot_I[(j << 1)][1] = sqrt_out * (-0.5f);
				arroot_I[(j << 1) + 1][0] = val0;
				arroot_I[(j << 1) + 1][1] = sqrt_out * 0.5f;
#else
				arroot_I[2 * j][0] = val0;
				arroot_I[2 * j][1] = (-1)*((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
				arroot_I[2 * j + 1][0] = val0;
				arroot_I[2 * j + 1][1] = ((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
#endif
			}
			
		}
		
		for (k = 0; k <= LOOPMAX; k++)
		{
			for (i = 0; i <= m; i++)
			{
				if (i == 0)
				{
					b[i] = buffer_b[i];
					c[i] = b[i];
				}
				else if (i == 1)
				{
					b[i] = buffer_b[i] + (r_last)*(b[i - 1]);
					c[i] = b[i] + (r_last)*(c[i - 1]);
				}
				else
				{
					b[i] = buffer_b[i] + (r_last)*(b[i - 1]) + (s_last)*(b[i - 2]);
					c[i] = b[i] + (r_last)*(c[i - 1]) + (s_last)*(c[i - 2]);
				}
				
			}
			if ((c[m - 1] * c[m - 3] - c[m - 2] * c[m - 2]) == 0)
			{
				dr = 0;
				ds = 0;
				}
				else
				{
					
			
				dr = ((b[m - 1])*(c[m - 2]) - (b[m])*(c[m - 3])) / ((c[m - 1])*(c[m - 3]) - (c[m - 2])*(c[m - 2]));
			
				ds = ((b[m])*(c[m - 2]) - (b[m - 1])*(c[m - 1])) / ((c[m - 1])*(c[m - 3]) - (c[m - 2])*(c[m - 2]));
			}
			
			//if (_isnan(dr) == 1 && _isnan(ds) == 1)
			if (isnan(dr) == 1 && isnan(ds) == 1)
			{
				break;
			}
			
			r = r_last + dr;
			s = s_last + ds;
			
			if ((fabs((ds / s)) < Accuracy) && (fabs((dr / r)) < Accuracy))
			{
				break;
			}
			
			r_last = r;
			s_last = s;
			
		}
		
		val0 = r * 0.5f;
		pre_cal = (float32_t)(r *r + 4 * s);
		if (pre_cal > 0)
		{
#if 01
			arm_sqrt_f32(pre_cal,&sqrt_out);
			arroot_R[(j << 1)] = val0 + (sqrt_out * 0.5f);
			arroot_R[(j << 1) + 1] = val0 - (sqrt_out * 0.5f);
#else
			arroot_R[2 * j] = val0 + ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
			arroot_R[2 * j + 1] = val0 - ((float32_t)sqrt(r *r + 4 * s) * 0.5f);
#endif
		}
		else if (pre_cal == 0)
		{
			arroot_R[(j << 1)] = val0;
			arroot_R[(j << 1) + 1] = val0;
		}
		else if (pre_cal < 0)
		{
#if 01
			arm_sqrt_f32((-1)*pre_cal,&sqrt_out);
			arroot_I[(j << 1)][0] = val0;
			arroot_I[(j << 1)][1] = sqrt_out * (-0.5f);
			arroot_I[(j << 1) + 1][0] = val0;
			arroot_I[(j << 1) + 1][1] = sqrt_out * 0.5f;
#else
			arroot_I[2 * j][0] = val0;
			arroot_I[2 * j][1] = (-1)*((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
			arroot_I[2 * j + 1][0] = val0;
			arroot_I[2 * j + 1][1] = ((float32_t)sqrt((-1)*(r *r + 4 * s)) * 0.5f);
#endif
		}
		
		m = m - 2;
		memcpy(buffer_b, b, (m + 1) *sizeof(float32_t));
	}
	
	if (n % 2 == 1)
	{
		arroot_R[n - 1] = (-1)*(b[1]) / (b[0]);
	}
	
	return;
}

void SortArray(float32_t arroot_I[][2])
{
#if 01
	int i, j;
	float32_t tmp;
	
	for (i = 0; i < M; i++)
	{
		for (j = (i + 1); j < M; j++)
		{
			if (arroot_I[i][0] > arroot_I[j][0])
			{
				tmp = arroot_I[i][0];
				arroot_I[i][0] = arroot_I[j][0];
				arroot_I[j][0] = tmp;
				
				tmp = arroot_I[i][1];
				arroot_I[i][1] = arroot_I[j][1];
				arroot_I[j][1] = tmp;
			}
		}
	}
#else
	qsort(arroot_I, EQNSIZE*2, sizeof(float32_t), cmpfunc);
#endif
}

void SortArraySingle(float32_t *Formants, float32_t *BandWidth, int Size)
{
	int i, j;
	float32_t tmp;
	
	for (i = 0; i < Size; i++)
	{
		for (j = (i + 1); j < Size; j++)
		{
			if (Formants[i] > Formants[j])
			{
				tmp = Formants[i];
				Formants[i] = Formants[j];
				Formants[j] = tmp;
				
				tmp = BandWidth[i];
				BandWidth[i] = BandWidth[j];
				BandWidth[j] = tmp;
			}
		}
	}
}
