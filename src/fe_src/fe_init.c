#include "global_define.h"

#define FE_FRAMESIZE 256
#ifdef ENABLE_FE
void fe_init()
{
	Init_fe();
	init_vad();
	reset_AHDR();

	memset(&g_str_fe, 0, sizeof(fe_struct_t));
}

void formant_enhancement( void )
{
	int16_t i16_buf16[FE_FRAMESIZE];

	for (int j = 0; j < FE_FRAMESIZE; j++)
	{
		i16_buf16[j] = g_str_audio.ui16_save_buf[g_str_fe.ui16_frame_no % FE_BUFFER][j<<1];
	}

	
	if( g_str_bitflag.b1_fe_enable )
	{
		FormantEnhancement(i16_buf16, 2);

#if 1
		if (AcousticHighDynamicRange_exe(i16_buf16, FE_FRAMESIZE) != 0)
		{
			for (int j = 0; j < FE_FRAMESIZE; j++)
			{
				g_str_fe.ui16_data[g_str_fe.ui16_frame_no % FE_BUFFER][j<<1] = 0;
			}
		}
		else
#endif
		{
			for (int j = 0; j < FE_FRAMESIZE; j++)
			{
				g_str_fe.ui16_data[g_str_fe.ui16_frame_no % FE_BUFFER][j<<1] = (uint16_t)i16_buf16[j];
				g_str_fe.ui16_data[g_str_fe.ui16_frame_no % FE_BUFFER][(j<<1)+1] = (uint16_t)i16_buf16[j];
			}
		}
	}
	else
	{
		for (int j = 0; j < FE_FRAMESIZE; j++)
		{
			g_str_fe.ui16_data[g_str_fe.ui16_frame_no % FE_BUFFER][j<<1] = (uint16_t)i16_buf16[j];
			g_str_fe.ui16_data[g_str_fe.ui16_frame_no % FE_BUFFER][(j<<1)+1] = (uint16_t)i16_buf16[j];
		}
	}
	
	if( g_str_bitflag.b2_fe_start_flag == 1 )	
	{
		if( g_str_fe.ui16_frame_no > FE_BUFFER>>1 )
		{
#ifdef ENABLE_AUDIO_DMA
			Audio_MAL_Play(g_str_fe.ui16_data,AUDIO_FRAME_SIZE*FE_BUFFER);
			AMP_STATUS_ENABLE;
#endif
			g_str_bitflag.b2_fe_start_flag = 2;
		}
	}
	g_str_fe.ui16_frame_no++;

}
#endif